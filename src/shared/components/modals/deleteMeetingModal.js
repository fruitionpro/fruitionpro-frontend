import React,{ Component } from 'react';
import {
    Modal,
    ModalHeader,
    ModalBody,
    FormGroup,
    Label,
    Input,
    Row,
    Col,
    Button
  } from "reactstrap";

class DeleteMeetingModal extends Component {

    onSelectDeleteType = (value) => {
      const { onsetDeleteType,toggle } = this.props;
        // console.log(value);
        onsetDeleteType(value);
        toggle()
    }

    render(){
        const { isOpen,toggle} = this.props;
        return (
            <Modal
            isOpen={isOpen}
            toggle={toggle}
          >
            <ModalHeader toggle={toggle} style={{ textAlign:'center'}}>
            Delete Meeting
            </ModalHeader>
            <ModalBody>
              <div className="freeTrial">
                <h3 className="heading">Do you want to delete this meeting or subsequent recurring meetings.</h3>
              </div>
    
              <div className="form-group text-center mt-4">
                <Button
                  className="trialBtn bs-planw"
                  onClick={() => {
                    this.onSelectDeleteType('only')
                  }}
                >
                  Only this
                </Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <Button
                  className="trialBtn bs-planw"
                  onClick={() => {
                    this.onSelectDeleteType('all')
                  }}
                >
                  Delete All
                </Button>
              </div>
            </ModalBody>
          </Modal>
        )
    }
}

export default DeleteMeetingModal;
