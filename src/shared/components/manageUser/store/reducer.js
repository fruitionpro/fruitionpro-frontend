import { SET_USER_ORGANIZATION_DATA, ORGANIZATION_INVITE_USER, UPDATE_USER, DELETE_USER, RESEND_EMAIL } from "../../../store/actionTypes";
import { composeResetReducer } from '../../../store/reducers/reset-reducer';
import modal from '../../../util/index';

const initialState = {
  manageUsers : [],
  inviteSent : null
}


const ManageUserReducer = (state=initialState,action) => {
  switch (action.type) {
    case SET_USER_ORGANIZATION_DATA.SUCCESS :{
      const { status, users } = action.payload.data;
      if (status === 200) {
        return {
          ...state,
          manageUsers: users,
        }
      }
      else {
        return state
      }
    }

    case ORGANIZATION_INVITE_USER.SUCCESS:{
      const { status,message} = action.payload.data;
      modal(status, message)

      if(status===200){
        return{
          ...state,
          inviteSent: true
        }
      }
      else{
        return state;
      }
    }

    case UPDATE_USER.SUCCESS : {
      const { status,message } = action.payload.data;
      modal(status,message);
      if(status===200){

        return{
          ...state,
        }
      }
      else{
        return state;
      }
    }

    case DELETE_USER.SUCCESS : {
      const { status,message } = action.payload.data;
      modal(status,message);
      if(status===200){
        return{...state}
      }
      else{
        return state;
      }
    }
  
    case RESEND_EMAIL.SUCCESS : {
      const{ status,message} = action.payload.data;
      modal(status, message);
      if (status === 200) {
        return { ...state }
      }
      else {
        return state;
      }
    }
    default:
     return state
  }
}

export default composeResetReducer(ManageUserReducer, initialState)
