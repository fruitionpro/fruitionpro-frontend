import React, { Component } from "react";
import {
  Button,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Label,
  Modal,
  ModalHeader,
  ModalBody
} from "reactstrap";
// import Avatar from "../../../asset/images/icons/Avatar.png";
import CalenderIcon from "../../../asset/images/dashboard-icons/small-calendar.svg";
import ClockIcon from "../../../asset/images/dashboard-icons/clock.svg";
import PlaceholderIcon from "../../../asset/images/dashboard-icons/placeholder.svg";
// import AddButtonIcon from "../../../asset/images/dashboard-icons/add-button.svg";
import AddButtonIcon from "../../icons/addIcon";
import PDFIcon from "../../../asset/images/icons/PDF_icon.png";
import Edit from "../../icons/moreIcon";
import PlusIcon from "../../../asset/images/icons/PlusIcon.png";
//import Select from 'react-select';
import Select from "react-select/creatable";
import InviteUserItems from "./options/inviteUserItems";
import moment from "moment";
import { onFileUpload } from "../../util/fileUpload";
import AddAgendaModal from "./agendaModal";
import { Redirect } from "react-router-dom";
import _ from "lodash";
import Loader from "react-loader-spinner";
import MsgModal from "../../util/index";
import MeetingEdit from "../modals";
import MeetingsPdf from "../createMeeting/pdf";
import DropdownShareOptions from "./dropdownShareOptions";
import { handleDocDownload, handlePdfDownload } from "../../util/pdf";
import { getFormattedDates } from "../../util/reccurrence-converter";
import { renderToString } from "react-dom/server";
import TickIcon from "../../../asset/images/icons/right-icon.svg";
import RedCross from "../../../asset/images/icons/cross-icon.svg";
import LockIcon from "../../../asset/images/dashboard-icons/lock.svg";
import CloseModal from "./meetingCloseModal";
import EditIcon from "../../../asset/images/icons/Edit.png";

export const circleCharacter = data => {
  if (!data) return;
  if (!data.name && !data.email) return;
  let nameArr;
  if (data && data.name) {
    nameArr = data.name.split(" ");
    if (data.logo) {
      return (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            position: "relative"
          }}
          className="circleBox"
        >
          <img
            src={data.logo}
            alt=""
            style={{
              width: "38px",
              height: "38px",
              borderRadius: "100%",
              position: "absolute"
            }}
            onerror="this.style.display='none'"
          />
          {nameArr.length >= 2 ? (
            <div>
              {(
                nameArr[0].charAt(0) + nameArr[nameArr.length - 1].charAt(0)
              ).toUpperCase()}
            </div>
          ) : (
              <div>
                {(nameArr[0].charAt(0) + nameArr[0].charAt(1)).toUpperCase()}
              </div>
            )}
        </div>
      );
    }
    if (nameArr.length >= 2) {
      return (
        nameArr[0].charAt(0) + nameArr[nameArr.length - 1].charAt(0)
      ).toUpperCase();
    } else {
      return (nameArr[0].charAt(0) + nameArr[0].charAt(1)).toUpperCase();
    }
  } else {
    return (data && data.email.charAt(0) + data.email.charAt(1)).toUpperCase();
  }
};

export default class CreateMeeting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdownOpen: false,
      isOpenAgendaModal: false,
      inviteModal: false,
      meetingNotes: props.meetingNotes || "",
      isCreated: false,
      isUpdated: false,
      openedValue: "",
      selectedParticipants: [],
      contacts: [],
      isUploading: false,
      agendaData: null,
      isUpdateAgenda: false,
      // isAttented: {},
      meetingModalClose: null,
      isCloseModal: false,
      isOpenMeetingModal: false,
      meetingParticipants: []
    };
    this.meetingEditRef = React.createRef();
    this.toggle = this.toggle.bind(this);
    this.modaltoggle = this.modaltoggle.bind(this);
    this.InviteModalToggle = this.InviteModalToggle.bind(this);
    this.agendaRef = React.createRef();
  }
  componentWillMount() {
    window.scrollTo(0, 0);
    const { meetingsDetails, addedParticipants } = this.props;
    const contactData = _.unionWith(
      meetingsDetails.contacts,
      addedParticipants,
      (a, b) => a.email === b.email
    );
    this.setState({
      contacts: contactData,
      meetingParticipants: addedParticipants
    });
  }

  toggle(value) {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen,
      openedValue: value
    }));
  }
  modaltoggle(callback) {
    this.setState(prevState => ({
      isOpenAgendaModal: !prevState.isOpenAgendaModal,
      agendaData: null
    }));
    if (callback) {
      callback();
    }
  }

  InviteModalToggle = () => {
    const { addedParticipants } = this.props;
    this.setState(prevState => ({
      inviteModal: !prevState.inviteModal,
      error: "",
      selectedParticipants:
        !prevState.inviteModal && addedParticipants && addedParticipants.length
          ? addedParticipants
          : []
    }));
  };

  handleAddDocuments = e => {
    const fileName = e.target.files[0].name;
    const { setDocuments, meetingDocuments } = this.props;
    let fileIndex = -1;
    if (meetingDocuments && meetingDocuments.length > 0) {
      fileIndex = meetingDocuments.findIndex(doc => doc.name === fileName);
    }
    if (fileIndex < 0) {
      this.setState({ isUploading: true });
      onFileUpload(e.target.files[0]).then(res => {
        if (res) {
          this.setState({ isUploading: false });
          setDocuments({ url: res, name: fileName });
        }
      });
    } else {
      MsgModal(400, "Document Already added");
    }
  };

  handleParticipants = () => {
    const { setParticipants } = this.props;
    const { selectedParticipants } = this.state;
    // if (selectedParticipants) {
    //   let participants = []
    //   selectedParticipants.forEach(item => {
    //     participants.push(item.data)
    //   })
    setParticipants(selectedParticipants);
    this.InviteModalToggle();
    //}
  };
  updateEditPermission = (item, canEdit) => {
    const { selectedParticipants } = this.state;
    const selected = [...selectedParticipants];
    const updateItem = selected.find(added => added.email === item.email);
    if (updateItem) {
      updateItem.canEdit = canEdit;
    }
    this.setState({ selectedParticipants: selected });
  };

  getFormattedParticipantsData = () => {
    const { selectedParticipants, contacts } = this.state;
    if (contacts && contacts.length > 0) {
      let paricipants = [];
      contacts.forEach(item => {
        const isAddedIndex = selectedParticipants.findIndex(
          added => added.email === item.email
        );
        const dataItem = {
          value: item.email,
          data: item,
          label: (
            <InviteUserItems
              updateEditPermission={this.updateEditPermission}
              canEdit={
                isAddedIndex > -1
                  ? selectedParticipants[isAddedIndex].canEdit
                  : false
              }
              showMenu={isAddedIndex > -1}
              data={item}
              label={item.email || item.name}
            />
          )
        };
        paricipants.push(dataItem);
      });
      return paricipants;
    }
    return [];
  };

  seletedView = () => {
    const { selectedParticipants } = this.state;
    if (selectedParticipants && selectedParticipants.length > 0) {
      let paricipants = [];
      selectedParticipants.forEach(item => {
        const dataItem = {
          value: item.email,
          data: item,
          label: item.email
        };
        paricipants.push(dataItem);
      });
      return paricipants;
    }
    return [];
  };

  seletedAttendedParticipants = () => {
    const { selectedParticipants } = this.state;
    if (selectedParticipants && selectedParticipants.length > 0) {
      let participants = [];
      selectedParticipants.forEach(item => {
        if (item.can_attend === "yes") {
          const dataItem = {
            value: item.email,
            data: item,
            label: item.email
          };
          participants.push(dataItem);
        }
      });
      return participants;
    }
    return [];
  };

  handleCreateMeeting = share => {
    const {
      addMeeting,
      createMeetingData,
      agendaItems,
      meetingDocuments,
      addedParticipants,
      userDetails,
      organizationData,
      resetMeetingData,
      private_notes,
      logo,
      togglePrivateNote
    } = this.props;
    const { meetingNotes } = this.state;
    togglePrivateNote(false);
    let meetingDates = [];
    let createFirstMeeting = true;
    if (createMeetingData.recurrenceData) {
      meetingDates = getFormattedDates(createMeetingData);
      const meetingStartDate = new Date(createMeetingData.startDateTime);
      meetingDates.forEach(dd => {
        const firstDate = new Date(dd.start_date);
        firstDate.setHours(
          meetingStartDate.getHours(),
          meetingStartDate.getMinutes(),
          meetingStartDate.getSeconds(),
          meetingStartDate.getMilliseconds()
        );
        if (firstDate.getTime() === meetingStartDate.getTime()) {
          createFirstMeeting = false;
        }
      });
    }

    meetingDates.sort(function (left, right) {
      return moment.utc(left.start_date).diff(moment.utc(right.start_date));
    });
    createMeetingData.recurrenceData = meetingDates;
    addMeeting({
      ...createMeetingData,
      agendas: agendaItems,
      documents: meetingDocuments,
      share,
      participants: addedParticipants,
      owner: userDetails.name || "",
      company: organizationData.organizationName,
      notes: meetingNotes,
      private_notes,
      logo,
      createFirstMeeting
    }).then(result => {
      const { status, message } = result.payload.data;
      if (status === 200) {
        this.setState({
          isCreated: true
        });
        MsgModal(status, message);
        resetMeetingData();
      }
    });
  };

  handleUpdate = (share, isShareOpen) => {
    const {
      createMeetingData,
      agendaItems,
      meetingDocuments,
      addedParticipants,
      userDetails,
      organizationData,
      updateMeeting,
      meetingId,
      private_notes,
      logo,
      togglePrivateNote,
      dataToPrint,
      setMeetingsType
    } = this.props;
    const { meetingNotes, selectedParticipants } = this.state;
    const contactData = _.unionWith(
      selectedParticipants,
      addedParticipants,
      (a, b) => a.email === b.email
    );
    togglePrivateNote(false);
    updateMeeting({
      ...createMeetingData,
      agendas: agendaItems,
      documents: meetingDocuments,
      participants: contactData,
      private_notes,
      email: userDetails.email || "",
      notes: meetingNotes,
      _id: meetingId,
      owner: userDetails.name || "",
      company: organizationData.organizationName || "",
      share,
      logo,
      isEditAll : dataToPrint && dataToPrint.editAllMeetings
    }).then(result => {
      const { status } = result.payload.data;
      if (status === 200) {
        setMeetingsType('upcoming');
        if (isShareOpen) {
          this.setState({
            isUpdated: false
          });
        } else {
          this.setState({
            isUpdated: true
          });
        }
      }
    });
  };

  toggleEditMeeting = () => {
    const { canEdit } = this.props;
    if (this.meetingEditRef.openModal && canEdit) {
      this.meetingEditRef.openModal();
    }
  };

  handleSummaryDownload = type => {
    const { createMeetingData, dataToPrint } = this.props;
    const name = createMeetingData.title || Date.now().toString();
    const file_name = name.replace(/[^A-Z0-9]+/gi, "_");
    const html = renderToString(
      <html>
        <head>
          <link
            href="https://fonts.googleapis.com/css?family=Raleway:400,500,700,800&display=swap"
            rel="stylesheet"
          />
        </head>
        <body>
          <MeetingsPdf type={type} dataToPrint={dataToPrint} />
        </body>
      </html>
    );
    if (type === "pdf") {
      handlePdfDownload(html, name);
    } else {
      handleDocDownload(html, file_name);
    }
  };

  handleSharePdf = (emails, callback) => {
    const {
      createMeetingData,
      dataToPrint,
      sharePdf,
      userDetails
    } = this.props;
    // const name = createMeetingData.title || Date.now().toString()
    // const file_name = name.replace(/[^A-Z0-9]+/gi, "_");
    const html = renderToString(
      <html>
        <body>
          <head>
            <link
              href="https://fonts.googleapis.com/css?family=Raleway:400,500,700,800&display=swap"
              rel="stylesheet"
            />
          </head>
          <MeetingsPdf dataToPrint={dataToPrint} />
        </body>
      </html>
    );
    const payload = {
      // meeting: createMeetingData.title,
      meeting_id: createMeetingData.meetingId,
      emails,
      attachment: `${html}`,
      sender_email: userDetails.email
      // file_name
    };
    sharePdf(payload).then(res => {
      if (res) {
        callback();
      }
    });
  };

  toggleMeetingModal = type => {
    const { addedParticipants } = this.props;
    this.setState(prevState => ({
      [type]: !prevState[type],
      selectedParticipants: !prevState[type] ? addedParticipants : []
    }));
  };

  handleMeetingOpenClose = (type, modalType) => {
    const { meetingId, setMeetingStatus } = this.props;
    if (type === 'close') {
      const obj = {
        meetingStatus: type,
        id: meetingId
      };
      setMeetingStatus(obj);
    }
    this.toggleMeetingModal(modalType);
    if (type === "close") {
      this.handleUpdate(false, "isShareOpen");
      if (this.refs.isShareMeeting) {
        this.refs.isShareMeeting.shareToggle();
      }
    } else this.handleUpdate(false);
  };

  isFiveMintuesToStart = () => {
    const { meetings, meetingId } = this.props;
    let meetingData =
      meetings &&
      meetings.length > 0 &&
      meetings.find(meeting => {
        return meeting._id === meetingId;
      });
    let startMeetingDate = moment
      .utc(meetingData && meetingData.start_date_time)
      .toDate();
    const a = moment(startMeetingDate);
    const b = moment();
    const c = moment.duration(a.diff(b)).asMinutes();
    if (c <= 5) {
      return true;
    }
    return false;
  };

  isMeetingTimeOver = dateTime => {
    const a = moment(dateTime);
    const b = moment();
    const c = moment.duration(b.diff(a)).asMinutes();
    if (c > 0) return true;
    else return false;
  };

  handleHeaderClick = () => {
    const { meetingStatus } = this.props;
    const { meetingModalClose } = this.state;
    if (meetingStatus === "close") {
      this.setState({ meetingModalClose: !meetingModalClose });
    } else {
      this.toggleEditMeeting();
    }
  };

  updateMeetingStartEndStatus = status => {
    const { updateStartEndStatus, meetingId } = this.props;
    updateStartEndStatus({ id: meetingId, status });
  };

  meetingAttendance = () => {
    const {
      meetingAttendance,
      userDetails,
      createMeeting_id,
      createMeetingData,
      setAttendance
    } = this.props;
    const obj = {
      attendee_email: userDetails.email,
      meeting_id: createMeeting_id,
      admin_email: createMeetingData.email
    };
    meetingAttendance(obj).then(result => {
      if (result) {
        MsgModal(result.payload.data.status, result.payload.data.message);
      }
      const { status } = result.payload.data;
      if (status === 200) {
        setAttendance(userDetails.email);
      }
      // createMeetingData.attendees.forEach(element => {
      //   if (element._id.email === userDetails.email) { element.is_present = true; return }
      // })
    });
  };

  toggleMeetingSaveModal = type => {
    const { addedParticipants } = this.props;
    this.setState(prevState => ({
      [type]: !prevState[type],
      selectedParticipants: !prevState[type] ? addedParticipants : []
    }));
  };

  isAttendedMeeting = () => {
    const { userDetails, addedParticipants } = this.props;
    // eslint-disable-next-line no-unused-vars
    if (addedParticipants && addedParticipants.length > 0) {
      for (const element of addedParticipants) {
        if (element.email === userDetails.email && element.is_present) {
          return true;
        }
      }
    }
    // if (createMeetingData && createMeetingData.attendees && createMeetingData.attendees.length > 0) {
    //   for (const element of createMeetingData.attendees) {
    //     if ((element._id.email === userDetails.email) && element.is_present) {
    //       return true
    //     }
    //   }
    // }
    return false;
  };

  attendanceOptions = () => {
    const { meetingParticipants } = this.state;
    if (meetingParticipants && meetingParticipants.length > 0) {
      let paricipants = [];
      meetingParticipants.forEach(item => {
        const dataItem = {
          value: item.email,
          data: item,
          label: item.email
        };
        paricipants.push(dataItem);
      });
      return paricipants;
    }
    return [];
  };

  render() {
    const {
      meetingModalClose,
      isOpenAgendaModal,
      meetingNotes,
      isCreated,
      openedValue,
      isUploading,
      agendaData,
      isUpdated,
      isUpdateAgenda,
      contacts,
      isCloseModal,
      isSavedModal,
      isOpenMeetingModal
    } = this.state;
    const {
      status,
      meetingId,
      meetingStatus,
      createMeetingData,
      setAgendaItems,
      agendaItems,
      meetingDocuments,
      addedParticipants,
      deleteDocument,
      canEdit,
      isCreate,
      updateAgendaItem,
      meetingsDetails,
      dataToPrint,
      userDetails,
      setMeetingStatus,
      isAdmin
    } = this.props;
    let stillUtcStart = moment.utc(createMeetingData.startDateTime).toDate();
    // let stillUtcEnd = moment.utc(createMeetingData.endDateTime).toDate();
    let startDate = moment(stillUtcStart)
      .local()
      .format("YYYY-MM-DD HH:mm:ss");
    const isReadyToStart = this.isFiveMintuesToStart();
    const isReadyToStop = this.isMeetingTimeOver(createMeetingData.endDateTime);
    // let endDate = moment(stillUtcEnd).local().format('YYYY-MM-DD HH:mm:ss');
    const isAttended = this.isAttendedMeeting();

    return (
      <>
        <div className="dashboard-wrapper add-agenda-screen">
          <div className="dashboard-content">
            {/* <CloseModal onDone={null} title={"Opps"} message={"This meeting is closed and only allows you to handle your own tasks. Reopen the meeting if you want to add, edit or delete the meeting content."} isOpen={meetingModalClose} toggle={() => this.toggleMeetingModal("meetingModalClose")} meetingStatus={dataToPrint.meetingStatus} /> */}
            <CloseModal
              onDone={() =>
                this.handleMeetingOpenClose("close", "isCloseModal")
              }
              title="Are you sure want to close meeting?"
              isOpen={isCloseModal}
              toggle={() => this.toggleMeetingModal("isCloseModal")}
              meetingStatus={dataToPrint.meetingStatus}
            ></CloseModal>
            <CloseModal
              onDone={() =>
                this.handleMeetingOpenClose("saveAndEnd", "isSavedModal")
              }
              title="Select participants who attended meeting"
              isOpen={isSavedModal}
              toggle={() => this.toggleMeetingModal("isSavedModal")}
              meetingStatus={dataToPrint.meetingStatus}
            >
              {/* <h6>Select participants who attended meeting</h6> */}
              <Select
                isMulti
                // menuIsOpen
                hideSelectedOptions={true}
                value={this.seletedAttendedParticipants()}
                onChange={data => {
                  let participants = [];
                  //this.seletedAttendedParticipants()
                  if (data) {
                    data.forEach(item => {
                      item.data.can_attend = "yes";
                      item.data.is_present = true;
                      participants.push(item.data);
                    });
                  }
                  this.setState({ selectedParticipants: participants });
                }}
                name="attendance"
                options={this.attendanceOptions()}
                className="basic-multi-select"
                classNamePrefix="invite"
                onCreateOption={value => {
                  const { contacts, selectedParticipants } = this.state;
                  const oldOptions = [...contacts];
                  const index = oldOptions.findIndex(
                    item => item.email === value
                  );
                  const selectedOld = [...selectedParticipants];
                  const selectedIndex = selectedOld.findIndex(
                    item => item.email === value
                  );
                  if (index <= -1) {
                    oldOptions.push({ email: value, can_attend: "yes" });
                    this.setState({ contacts: oldOptions });
                    if (selectedIndex <= -1) {
                      selectedOld.push({ email: value, can_attend: "yes" });
                      this.setState({ selectedParticipants: selectedOld });
                    }
                  }
                }}
                createOptionPosition="first"
                isValidNewOption={(i, v, op) => {
                  const { contacts } = this.state;
                  let re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                  const index = contacts.findIndex(
                    element => element.email === i
                  );
                  let isNotExist = true;
                  if (index > -1) {
                    isNotExist = false;
                  }
                  return re.test(String(i).toLowerCase()) && isNotExist;
                }}
              />
            </CloseModal>
            <CloseModal
              onDone={() =>
                this.handleMeetingOpenClose("open", "isOpenMeetingModal")
              }
              rightAction={() => this.toggleMeetingModal("isOpenMeetingModal")}
              title="Reopen Meeting"
              message="Are you sure want to reopen this meeting"
              isOpen={isOpenMeetingModal}
              toggle={() => this.toggleMeetingModal("isOpenMeetingModal")}
              meetingStatus={dataToPrint.meetingStatus}
            />
            {(isCreated || isUpdated) && <Redirect to={"/view"} />}
            {!canEdit && (
              <div className="agenda-header">
                <h1 className="title">Meeting summary</h1>
              </div>
            )}
            <div className="agenda-header">
              <div className="agenda-wrap">
                <h1 className="title  shares-title">
                  {createMeetingData.title || ""}
                  <span className="ml-2">
                    {meetingStatus === "close" && (
                      <img src={LockIcon} alt="noImg" />
                    )}
                  </span>
                </h1>
                <div className="agenda-dropdown">
                  <span className="share-dropdown">
                    <DropdownShareOptions
                      ref="isShareMeeting"
                      setModal={modalType => this.toggleMeetingModal(modalType)}
                      isReadyToStop={isReadyToStop}
                      meetingId={meetingId}
                      setMeetingStatus={setMeetingStatus}
                      meetingData={dataToPrint}
                      userDetails={userDetails}
                      addedParticipants={addedParticipants}
                      contacts={meetingsDetails.contacts}
                      handleSharePdf={this.handleSharePdf}
                      handleSummaryDownload={this.handleSummaryDownload}
                    />
                  </span>
                </div>
              </div>
            </div>
            <div className="dashboard-listWrap">
              <ul className="list-inline header-list">
                <li>
                  <img alt="" className="list-icon" src={CalenderIcon} />
                  <span>
                    {moment(startDate).format("dddd, MMMM DD, YYYY")}
                  </span>{" "}
                </li>
                <li>
                  <img alt="" className="list-icon" src={ClockIcon} />
                  <span>{moment(startDate).format("h: mm: A")}</span>
                </li>
                <li>
                  <img
                    alt=""
                    className="list-icon navigation"
                    src={createMeetingData.location ? PlaceholderIcon : null}
                  />
                  <span>{createMeetingData.location || ""}</span>
                </li>
                {meetingStatus !== "close" &&
                  isReadyToStart &&
                  dataToPrint.status !== "completed" && (
                    <li className="startMeetBtn">
                      {isAdmin ? (
                        <button
                          className={`${(dataToPrint.status === "started" ||
                            isReadyToStop) &&
                            "disable"}`}
                          disabled={
                            dataToPrint.status === "started" || isReadyToStop
                          }
                          onClick={() =>
                            this.updateMeetingStartEndStatus("started")
                          }
                        >
                          start Meeting
                        </button>
                      ) : (
                          dataToPrint.status === "started" && (
                            <button
                              className={`${isAttended && "disable"}`}
                              disabled={isAttended}
                              onClick={() => this.meetingAttendance()}
                            >
                              I'm attending
                          </button>
                          )
                        )}
                    </li>
                  )}

                {/* {meetingStatus !== 'close' && dataToPrint.status === 'started' && <li className="startMeetBtn"><button onClick={() => this.updateMeetingStartEndStatus("completed")}>End Meeting</button></li>}
            {meetingStatus !== 'close' && dataToPrint.status === 'completed' && <li className="startMeetBtn"><button >Completed</button></li>} */}
              </ul>
              <div className="mb-4">
                {(canEdit || isCreate) &&
                  meetingStatus !== "close" &&
                  status !== "started" && (
                    <button
                      onClick={this.handleHeaderClick}
                      className="edit-meetingBtn"
                    >
                      Edit Meeting
                      <span className="ml-2">
                        <img src={EditIcon} alt="" />
                      </span>
                    </button>
                  )}
              </div>
            </div>
            <MeetingEdit
              isEditMode={true}
              getRef={ref => (this.meetingEditRef = ref)}
            />
            {((agendaItems && agendaItems.length > 0) || canEdit) && (
              <div className="field-section">
                <h3 className="mb-4">Add Agenda</h3>
                <ul className="list-inline agenda-upload d-flex flex-wrap">
                  {agendaItems &&
                    agendaItems.length > 0 &&
                    agendaItems.map(item => {
                      return (
                        <li
                          onClick={() => {
                            this.setState(prevState => ({
                              isOpenAgendaModal: !prevState.isOpenAgendaModal,
                              agendaData: item,
                              isUpdateAgenda: true
                            }));
                          }}
                        >
                          <Label>
                            <p className="mb-0 text-center">
                              {item.title || ""}
                            </p>
                          </Label>
                        </li>
                      );
                    })}
                  {canEdit && (
                    <li>
                      <Label>
                        <AddButtonIcon />
                        <Button
                          className="agenda-addBtn"
                          onClick={() => {
                            if (meetingStatus === "close") {
                              this.setState({
                                meetingModalClose: !meetingModalClose
                              });
                            } else {
                              this.modaltoggle(null);
                            }
                          }}
                        ></Button>
                      </Label>
                    </li>
                  )}
                </ul>
              </div>
            )}
            <div className="field-section">
              <h3 className="mb-4">Add Participants</h3>
              <div className="image-upload">
                <ul className="list-inline image-upload">
                  {addedParticipants &&
                    addedParticipants.length > 0 &&
                    addedParticipants.map(item => {
                      return (
                        <li>
                          <Label title={item.email || ""} className="image">
                            <div className="assigneeUser">
                              <span
                                className="partImg"
                                style={{
                                  position: "absolute",
                                  top: "50%",
                                  left: "50%",
                                  transform: "translate(-50%, -50%)",
                                  borderRadius: "100%"
                                }}
                              >
                                {/* {(item.email.charAt(0).toUpperCase())} */}
                                {circleCharacter(item)}
                              </span>
                              <span className="online-user">
                                {/* <button style={{ background: 'none', border: 'none' }}> */}
                                {item.is_present === true && (
                                  <img src={TickIcon} alt="" />
                                )}
                                {item.is_present === false &&
                                  item.can_attend === "yes" && (
                                    <span className="greenCircle"></span>
                                  )}
                                {item.is_present === false &&
                                  item.can_attend === "maybe" && (
                                    <span className="orangeCircle"></span>
                                  )}
                                {item.is_present === false &&
                                  (item.can_attend === "no" ||
                                    item.can_attend === "") && (
                                    <img src={RedCross} alt="" />
                                  )}
                                {/* <img src={item.is_present === true ? TickIcon : RedCross} alt="" /> */}
                                {/* </button> */}
                              </span>
                            </div>
                          </Label>
                        </li>
                      );
                    })}

                  {canEdit && (
                    <li>
                      <Label>
                        <AddButtonIcon />
                        <button
                          onClick={() => {
                            if (meetingStatus === "close") {
                              this.setState({
                                meetingModalClose: !meetingModalClose
                              });
                            } else {
                              this.InviteModalToggle();
                            }
                          }}
                        ></button>
                      </Label>
                    </li>
                  )}
                </ul>
              </div>
            </div>

            {canEdit && (
              <div className="field-section">
                <h3>
                  Add Document
                  <span className="upload-pdf">
                    {canEdit && (
                      <Label>
                        {meetingStatus === "close" ? (
                          <span
                            onClick={() => {
                              this.setState({
                                meetingModalClose: !meetingModalClose
                              });
                            }}
                          >
                            <img src={PlusIcon} alt="noImg" />
                          </span>
                        ) : (
                            <span>
                              <input
                                onChange={e => {
                                  this.handleAddDocuments(e);
                                }}
                                accept="application/pdf"
                                type="file"
                              />
                              <img src={PlusIcon} alt="noImg" />
                            </span>
                          )}
                      </Label>
                    )}
                  </span>
                </h3>
                <ul className="list-inline document-upload">
                  {meetingDocuments &&
                    meetingDocuments.length > 0 &&
                    meetingDocuments.map(document => {
                      return (
                        <li>
                          <Label>
                            <div className="doc-content">
                              <a download href={document.url} target="_blank">
                                <img
                                  src={PDFIcon}
                                  alt="noImg"
                                  className="img-fluid"
                                />
                                <p title={document.name || ""}>
                                  {document.name || ""}
                                </p>
                              </a>
                            </div>
                            {canEdit && meetingStatus !== "close" && (
                              <Dropdown
                                isOpen={
                                  this.state.dropdownOpen &&
                                  document.url === openedValue
                                }
                                toggle={e => this.toggle(document.url)}
                                className="edit-icon"
                              >
                                <DropdownToggle>
                                  <Edit />
                                </DropdownToggle>
                                <DropdownMenu>
                                  <DropdownItem
                                    onClick={e => {
                                      if (meetingStatus === "close") {
                                        this.setState({
                                          meetingModalClose: !meetingModalClose
                                        });
                                      } else {
                                        deleteDocument(document);
                                      }
                                    }}
                                  >
                                    Delete
                                  </DropdownItem>
                                  {/* <DropdownItem divider />
                    <DropdownItem >Link to agenda item</DropdownItem> */}
                                </DropdownMenu>
                              </Dropdown>
                            )}
                          </Label>
                        </li>
                      );
                    })}
                  {isUploading && (
                    <li>
                      <div className="doc-content">
                        <span style={{ textAlign: "center" }}>
                          <Loader
                            type="TailSpin"
                            color="#00BFFF"
                            height={60}
                            width={60}
                          />
                          Uploading...
                        </span>
                      </div>
                    </li>
                  )}
                </ul>
              </div>
            )}
            <div className="field-section">
              <h3>Comments</h3>
              <textarea
                value={meetingNotes}
                onChange={e => {
                  if (meetingStatus === "close") {
                    this.setState({ meetingModalClose: !meetingModalClose });
                  } else {
                    this.setState({ meetingNotes: e.target.value });
                  }
                }}
                disabled={meetingStatus === "close"}
              ></textarea>

              {!isCreate && canEdit && meetingStatus !== "close" && (
                <div className="agenda-submitBtn">
                  {
                    <button
                      onClick={() => {
                        if (meetingStatus === "close") {
                          this.setState({
                            meetingModalClose: !meetingModalClose
                          });
                        } else {
                          this.handleUpdate(false);
                        }
                      }}
                      type="button"
                      className="btn btn-gradient"
                    >
                      Save
                    </button>
                  }
                  {meetingStatus !== "close" && (
                    <button
                      onClick={() => {
                        if (status === "started") {
                          this.toggleMeetingSaveModal("isSavedModal");
                        } else {
                          if (meetingStatus === "close") {
                            this.setState({
                              meetingModalClose: !meetingModalClose
                            });
                          } else {
                            this.handleUpdate(true);
                          }
                        }
                      }}
                      type="button"
                      className="btn btn-gradient"
                    >
                      {status === "started" ? "Save & End" : "Save & Share"}
                    </button>
                  )}
                </div>
              )}

              {isCreate && meetingStatus !== "close" && (
                <div className="agenda-submitBtn">
                  <button
                    onClick={() => {
                      if (meetingStatus === "close") {
                        this.setState({
                          meetingModalClose: !meetingModalClose
                        });
                      } else {
                        this.handleCreateMeeting(false);
                      }
                    }}
                    type="button"
                    className="btn btn-gradient"
                  >
                    Save
                  </button>
                  <button
                    onClick={() => {
                      if (meetingStatus === "close") {
                        this.setState({
                          meetingModalClose: !meetingModalClose
                        });
                      } else {
                        this.handleCreateMeeting(true);
                      }
                    }}
                    type="button"
                    className="btn btn-gradient"
                  >
                    Save & Share
                  </button>
                </div>
              )}
              {!isCreate && !canEdit && meetingStatus !== "close" && (
                <div className="text-center">
                  <button
                    onClick={() => this.handleUpdate(false)}
                    type="button"
                    className="btn btn-gradient mt-3"
                  >
                    Save
                  </button>
                </div>
              )}
            </div>

            {isOpenAgendaModal && (
              <AddAgendaModal
                userDetails={userDetails}
                meetingStatus={meetingStatus}
                status={status}
                isReadyToStart={isReadyToStart}
                ref={this.agendaRef}
                contacts={meetingsDetails.contacts}
                participants={addedParticipants}
                isCreate={isCreate}
                canEdit={canEdit}
                isUpdateAgenda={isUpdateAgenda}
                updateAgendaItem={updateAgendaItem}
                modaltoggle={this.modaltoggle}
                isOpen={true}
                setAgendaItems={setAgendaItems}
                agendaData={agendaData}
              />
            )}
            <Modal
              isOpen={this.state.inviteModal}
              toggle={this.InviteModalToggle}
              className="create-meeting-modal invite-modal"
            >
              <ModalHeader toggle={this.InviteModalToggle}>
                Invite<span onClick={this.handleParticipants}>Done</span>
              </ModalHeader>
              <ModalBody>
                <div className="invite-screen mb-5">
                  <h2>
                    Give other people to access to this meeting in minute.
                  </h2>
                  <p>
                    For every participant that signs up for minute through your
                    invite, you will receive 30 days of Premium service for
                    free!
                  </p>
                  <Select
                    isMulti
                    menuIsOpen
                    hideSelectedOptions={false}
                    value={this.seletedView()}
                    onChange={data => {
                      let participants = [];
                      if (data) {
                        data.forEach(item => {
                          participants.push(item.data);
                        });
                      }
                      this.setState({ selectedParticipants: participants });
                    }}
                    name="colors"
                    options={this.getFormattedParticipantsData()}
                    className="basic-multi-select"
                    classNamePrefix="invite"
                    formatCreateLabel={value => {
                      return <InviteUserItems label={value || ""} />;
                    }}
                    onCreateOption={value => {
                      const { contacts, selectedParticipants } = this.state;
                      const oldOptions = [...contacts];
                      const index = oldOptions.findIndex(
                        item => item.email === value
                      );
                      const selectedOld = [...selectedParticipants];
                      const selectedIndex = selectedOld.findIndex(
                        item => item.email === value
                      );
                      if (index <= -1) {
                        oldOptions.push({ email: value });
                        this.setState({ contacts: oldOptions });
                        if (selectedIndex <= -1) {
                          selectedOld.push({ email: value });
                          this.setState({ selectedParticipants: selectedOld });
                        }
                      }
                    }}
                    createOptionPosition="first"
                    isValidNewOption={(i, v, op) => {
                      const { contacts } = this.state;
                      let re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                      const index = contacts.findIndex(
                        element => element.email === i
                      );
                      let isNotExist = true;
                      if (index > -1) {
                        isNotExist = false;
                      }
                      return re.test(String(i).toLowerCase()) && isNotExist;
                    }}
                  />
                </div>
              </ModalBody>
            </Modal>
            {/* <MeetingsPdf dataToPrint={dataToPrint} /> */}
          </div>
        </div>
      </>
    );
  }
}
