import React, { Component } from "react";
import UserIcon from "../../../../asset/images/mom-icons/user.svg";
import { Link, withRouter } from "react-router-dom";
import { circleCharacter } from "../../createMeeting/createMeetingComponent";
import Modal from '../../../util/index';

class Notifications extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onNotification = data => {
    const {
      setTaskSummaryData,
      setMeetingSummaryData,
      history,
      userDetails
    } = this.props;
    if (data && data.type === "meeting" && userDetails) {
      if (data.meeting && data.meeting.title) {
        if (data.meeting && userDetails.email) {
          if (data.meeting.is_deleted === true) {
            Modal(400, "Your meeting has been cancelled.");
          } else {
            setMeetingSummaryData(data.meeting, userDetails.email);
            history.push("/view/meetings-details");
          }
        }
      }
    }
    if (data && data.type === "task") {
      if (data.task && data.task._id) {
        if (data.task.is_deleted === true) {
          Modal(400, "Your task has been deleted.");
        }else {
          setTaskSummaryData(data.task);
          history.push({
            pathname: "/view/tasks",
            search: `?tid=${data.task._id}`
          });
        }
      }
    }
    if (data && data.type === "profile") {
      history.push("/view/profile");
    }
    if (data && data.type === "invitation") {
      history.push("/view/manage-users");
    }
  };
  render() {
    const { notifications, history } = this.props;
    return (
      <div className="meetings-card">
        <div className="meeting-heading">
          <h2>
            Notifications
            <span className="ml-2">
              {(notifications && notifications.length) || 0}
            </span>
          </h2>
          <a onClick={() => history.push("/view/all-notifications")}>
            See all
          </a>
        </div>
        <ul className="meetings-list list-overflow notification-list">
          {notifications && notifications.length > 0 ? (
            notifications.map((data, index) => {
              if (index <= 3) {
                return (
                  <li onClick={() => this.onNotification(data)}>
                    <div className="meetings-user">
                      <div className="meeting-user-inner">
                        <div
                          title={
                            (data && data.sender && data.sender.email) || ""
                          }
                          className="taskUser-circle"
                        >
                          <span
                            className="inviteUserIcon"
                            style={{
                              position: "absolute",
                              top: "50%",
                              left: "50%",
                              transform: "translate(-50%, -50%)"
                            }}
                          >
                            {circleCharacter(data && data.sender)}
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className="meeting-content ml-2">
                      <div className="meeting-details lock notification-text">
                        <div className="notification-title">
                          <span
                            dangerouslySetInnerHTML={{
                              __html: `<p>${data && data.message.includes("<br>") ? data.message.split("<br>", 1)[0] + "..." : data.message}</p>`
                            }}
                          ></span>
                          <p>
                            <i>{(data && data.createdAt) || ""}</i>
                          </p>
                        </div>
                      </div>
                    </div>
                  </li>
                );
              }
            })
          ) : (
              <div className="no-meeting-list">
                <p className="mb-0">No Notifications</p>
              </div>
            )}
        </ul>
      </div>
    );
  }
}

export default withRouter(Notifications);
