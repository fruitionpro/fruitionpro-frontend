import React, { useState } from "react";
import cx from "classnames";
import moment from "moment";
import DatePicker from "react-datepicker";
import CalendarIcon from "../../../../../../asset/images/virtual-assistant/calendar.png";

const DateTimeCard = (props) => {
  const { type } = props;
  const [startDate, setStartDate] = useState(new Date());
  const date = moment(startDate).format("d/mm/yyy");
  const time = moment(startDate).format("h:mm");
  return (
    <div className={cx("timerCard", type === "start" ? type : "end")}>
      <p className="timeLabel">
        {type === "start" ? "Start Time" : "End Time"}
        <span>
          <img src={CalendarIcon} alt="" />
        </span>
      </p>
      <div className="timer DTCardWrapper">
        <DatePicker
          selected={startDate}
          onChange={(date) => setStartDate(date)}
          showTimeSelect
          timeFormat="HH:mm"
          timeIntervals={15}
          timeCaption="time"
          dateFormat="MMMM d, yyyy h:mm aa"
          popperPlacement="left"
        />
        <p className="time">{time}</p>
        <p className="date">{date}</p>
      </div>
    </div>
  );
};
export default DateTimeCard;
