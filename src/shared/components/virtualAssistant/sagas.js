import { fork, take, put, call } from "redux-saga/effects";
import { TEST } from "./constants";

function* watchTest() {
  while (true) {
    try {
        yield take(TEST);
      console.log("here in saga")
    } catch (err) {
        console.log(err);
    }
  }
}

export default function* root() {
  yield fork(watchTest);
}