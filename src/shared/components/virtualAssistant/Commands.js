export default class Commands {
  constructor(instance) {
    this.instance = instance;
  }

  injectCommands() {
    let VA = this.instance;
    const commands = getCommands(VA)();
    VA.addCommands([...commands.general]);
  }
}

function getCommands(VA) {
  return function () {
    return {
      general: [
        {
          description: "Restarts VA with the initial configuration",
          indexes: ["Restart yourself", "Shutdown"],
          action: (i) => {
            if (i === 0) {
              VA.restart().then(() => {
                VA.say("Succesfully restarted sir");
              });
            } else {
              VA.say("Goodbye sir");
              VA.fatality();
            }
          },
        },
        {
          description: "General talks",
          indexes: [
            "Hello",
            "How are you?",
            "Who are you?",
            "How many meetings are scheduled today?",
          ],
          action: (i) => {
            switch (i) {
              case 0:
                VA.sayRandom([
                  "Good Morning",
                  "Hey, good to see you again",
                  "I don't have anything to say today",
                  "Did you remember that I didn't say nothing yesterday? Well, today I dont want neither.",
                ]);
                break;
              case 1:
                VA.say("I am good, How about you ?");
                break;
              case 2:
                VA.say("I am john, your assistant");
                break;
              case 3:
                VA.say("Boss, you have five scheduled meetings for today.");
                break;
              default:
                VA.say("pardon!");
            }
          },
        },
        {
          description:
            "If my database contains the name of a person say something",
          smart: true, // a Smart command allow you to use wildcard in order to retrieve words that the user should say
          // Ways to trigger the command with the voice
          indexes: [
            "Do you know who is *",
            "I don't know who is *",
            "Is * a good person",
          ],
          // Do something when the commands is triggered
          action: function (i, wildcard) {
            var database = ["Carlos", "Bruce", "David", "Joseph", "Kenny"];

            //If the command "is xxx a good person" is triggered do, else
            if (i == 2) {
              if (database.indexOf(wildcard.trim())) {
                VA.say("I'm a machine, I dont know what is a feeling");
              } else {
                VA.say(
                  "I don't know who is " +
                    wildcard +
                    " and i cannot say if is a good person"
                );
              }
            } else {
              if (database.indexOf(wildcard.trim())) {
                VA.say(
                  "Of course i know who is " +
                    wildcard +
                    ". A really good person"
                );
              } else {
                VA.say(
                  "My database is not big enough, I don't know who is " +
                    wildcard
                );
              }
            }
          },
        },
      ],
    };
  };
}
