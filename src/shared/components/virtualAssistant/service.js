export const VAService = {
  testAPICall: async (ids) => {
    return await SuperFetch.get(`albums?ids=${ids}`);
  }
};