import { TEST, SET_VA } from './constants';

export const test = () => ({
	type: TEST,
});

export const setVA = (VA) => ({
	type: SET_VA,
	VA,
});
