import React from "react";
import { connect } from "react-redux";
import Listner from "./components/listner";
import { Button, Collapse } from "reactstrap";
import AssistantOptions from "./components/assistantOptions";
import AssistantMeetings from "./components/assistantMeetings";
// import ChatBot from "../../icons/chatBot";
// import CrossIcon from "../../icons/cross";
// import assistantMeetings from "./assistantMeetings";
import CloseIcon from "../../icons/close";
import CreateMeeting from "./components/createMeeting";
// import ContinueArrow from "../../../asset/images/virtual-assistant/continueArrow.png";
import withVA from "../../../hoc/VAsistant";
import { compose, bindActionCreators } from "redux";
import { test, setVA } from "./actions";
class VBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      activeStep: 1,
      fullSpeech: [],
    };
  }
  componentDidMount() {
    this.props.setVA(this.props.VA);
  }
  analyseSpeech = () => {
    const {
      fullSpeech: [lastCommand],
    } = this.state;
    const navigateTo = ["create", "meeting"].every((el) =>
      lastCommand.includes(el)
    )
      ? "create_meeting"
      : ["create", "task"].every((el) => lastCommand.includes(el))
      ? "create_task"
      : "";
    this.navigateTo(navigateTo);
  };
  getStep(which) {
    return {
      create_meeting: 3,
      create_task: 2,
    }[which];
  }
  navigateTo = (where) => {
    where && this.setState({ activeStep: this.getStep(where) });
  };
  componentDidUpdate(prevProps) {
    const { recognizedText } = this.props;
    if (prevProps.recognizedText !== recognizedText) {
      this.setState(
        (s) => ({
          fullSpeech: [recognizedText, ...s.fullSpeech],
        }),
        this.analyseSpeech
      );
    }
  }
  promisedSetState = (state) => {
    return new Promise((resolve) => {
      this.setState(state, () => {
        resolve();
      });
    });
  };
  toggleVI = async () => {
    const that = this;
    await this.promisedSetState((s) => ({ isOpen: !s.isOpen }));
    const {
      props: { startVA, stopVA },
      state: { isOpen },
    } = this;
    isOpen
      ? startVA()
      : (function () {
          stopVA();
          that.setState({ activeStep: 1 });
        })();
  };
  setActiveStep = (activeStep) => {
    this.setState({ activeStep });
  };
  render() {
    const { isOpen, activeStep, createMeeting, fullSpeech } = this.state;
    const { setActiveStep, toggleVI } = this;
    return (
      <>
        <Collapse isOpen={isOpen} className="virtual-assistant-main">
          <div className="virtual-assistant">
            <Listner isTop={activeStep > 1} />
            <div>
              {activeStep === 1 && (
                <AssistantOptions setActiveStep={setActiveStep} />
              )}
              {activeStep === 2 && (
                <AssistantMeetings setActiveStep={setActiveStep} />
              )}
              {activeStep === 3 && (
                <CreateMeeting fullSpeech={fullSpeech} {...createMeeting} />
              )}
              <div className="text-right">
                {/* <Button className="chatBotBtn">
                  <ChatBot />
                  {/* <CrossIcon /> */}
                {/* </Button> */}
                {/* <Button className="right-arrow">
                  <img src={ContinueArrow} alt="" />
                </Button> */}
              </div>
            </div>
          </div>
        </Collapse>
        <button onClick={toggleVI} className="virtual-assistant-toggle">
          <CloseIcon />
        </button>
      </>
    );
  }
}
const mapStateToProps = (state) => ({
  testProp: state.viReducer.testState,
});
const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
    ...bindActionCreators({ test, setVA }, dispatch),
  };
};
export default compose(
  withVA,
  connect(mapStateToProps, mapDispatchToProps)
)(VBox);
