import React from 'react';
import { Button, Row, Col } from 'reactstrap';
// import AddParticipants from "../addParticipants";
// import AddDocument from "../addDocument";

export default () => {
	return (
		<div className="assistant-meeting">
			<div>
				<Button className="subMeetingsBtn blue w-100 partBtn">
					<i className="fas fa-plus" />
					Add Participants
				</Button>
				<Button className="subMeetingsBtn red pl-3">
					Add Agenda
					<i className="fas fa-pencil-alt ml-2"></i>
				</Button>
				<Button className="subMeetingsBtn green pl-3">
					Add Document  <i className="fas fa-pencil-alt ml-2"></i>
				</Button>
				<Button className="subMeetingsBtn orange pl-3">
					Add Location
					<i className="fas fa-pencil-alt ml-2"></i>
				</Button>
			</div>
			{/* <AddParticipants /> */}
			{/* <AddDocument /> */}
		</div>
	);
};
