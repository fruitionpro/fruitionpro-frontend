import React from "react";
import { Button, Row, Col } from "reactstrap";
import UserImg from "../../../../../asset/images/virtual-assistant/user.jpg";

export default () => {
  return (
    <div className="assistant-meeting">
      <h5 className="list-title">Participants</h5>
      <div className="participantsUsers">
        <img src={UserImg} alt="" />
        <img src={UserImg} alt="" />
        <div className="add-user">
          <i className="fas fa-pencil-alt" />
        </div>
      </div>
      {/* <div>
        <Button className="subMeetingsBtn red pl-3">
          Add Agenda
          <i className="fas fa-pencil-alt ml-2"></i>
        </Button>
        <Button className="subMeetingsBtn green pl-3">
          Add Document  <i className="fas fa-pencil-alt ml-2"></i>
        </Button>

        <Button className="subMeetingsBtn orange pl-3">
          Add Location
          <i className="fas fa-pencil-alt ml-2"></i>
        </Button>
      </div> */}
      <div className="meetings-details">
        <div className="detailsCard red">
          <h4>Agenda</h4>
          <ul>
            <li>Reference site about Lorem Ipsum</li>
            <li>giving information on its origins.</li>
          </ul>
        </div>
        <div className="detailsCard green">
          <h4>Document</h4>
          <ul>
            <li>Screenshot 20... .png</li>
            <li>Screenshot 20... .png</li>
          </ul>
        </div>
        <div className="detailsCard orange">
          <h4>Location</h4>
          <ul>
            <li>Meeting Room 1</li>
          </ul>
        </div>
      </div>
      <div className="text-center pt-2">
        <Button className="doneBtn">Done</Button>
      </div>
    </div>
  );
};
