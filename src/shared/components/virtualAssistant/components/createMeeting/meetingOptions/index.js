import React, { useEffect, useState } from "react";
import { Button, Row, Col } from "reactstrap";
import AddParticipants from "../addParticipants";
import AddDocument from "../addDocument";
import VModal from "../../vModal";

export default () => {
  const [addParticipantsModal, setAddParticipantsModal] = useState(false);
  const [addDocumentsModal, setAddDocumentsModal] = useState(false);
  const toggleParticipants = () =>
    setAddParticipantsModal((previousState) => !previousState);
  const toggleDocuments = () =>
    setAddDocumentsModal((previousState) => !previousState);
  return (
    <div className="assistant-meeting">
      <div>
        <Button
          className="subMeetingsBtn blue w-100 partBtn"
          onClick={toggleParticipants}
        >
          <i className="fas fa-plus" />
          Add Participants
        </Button>
        <Button className="subMeetingsBtn red pl-3">
          Add Agenda
          <i className="fas fa-pencil-alt ml-2"></i>
        </Button>
        <Button className="subMeetingsBtn green pl-3" onClick={toggleDocuments}>
          Add Document  <i className="fas fa-pencil-alt ml-2"></i>
        </Button>
        <Button className="subMeetingsBtn orange pl-3">
          Add Location
          <i className="fas fa-pencil-alt ml-2"></i>
        </Button>
      </div>
      {addParticipantsModal && (
        <VModal title="Add Participants" toggle={toggleParticipants}>
          <AddParticipants />
        </VModal>
      )}
      {addDocumentsModal && (
        <VModal title="Add Documents" toggle={toggleDocuments}>
          <AddDocument />
        </VModal>
      )}
    </div>
  );
};
