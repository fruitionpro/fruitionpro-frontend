/* eslint-disable no-extend-native */
import React from "react";
import { Input, FormGroup, Label, Col, Row, Button } from "reactstrap";
import StartEndTime from "./startEndTime";
import AssistantDropdown from "../assistantDropdown";
import cx from "classnames";
import ContinueArrow from "../../../../../asset/images/virtual-assistant/continueArrow.png";
import MeetingOptions from "./meetingOptions";
// import Participants from "../participants";
const monthsRegx = /(Jan(uary)?|Feb(ruary)?|Mar(ch)?|Apr(il)?|May|Jun(e)?|Jul(y)?|Aug(ust)?|Sep(tember)?|Oct(ober)?|Nov(ember)?|Dec(ember)?)/g;
const addToStrPrototype = () => {
  String.prototype.regexIndexOf = function (regex, startpos) {
    var indexOf = this.substring(startpos || 0).search(regex);
    return indexOf >= 0 ? indexOf + (startpos || 0) : indexOf;
  };
  String.prototype.regexLastIndexOf = function (regex, startpos) {
    regex = regex.global
      ? regex
      : new RegExp(
          regex.source,
          "g" + (regex.ignoreCase ? "i" : "") + (regex.multiLine ? "m" : "")
        );
    if (typeof startpos == "undefined") {
      startpos = this.length;
    } else if (startpos < 0) {
      startpos = 0;
    }
    var stringToWorkWith = this.substring(0, startpos + 1);
    var lastIndexOf = -1;
    var nextStop = 0;
    let result;
    while ((result = regex.exec(stringToWorkWith)) != null) {
      lastIndexOf = result.index;
      regex.lastIndex = ++nextStop;
    }
    return lastIndexOf;
  };
};
class CreateMeeting extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      createMeeting: {
        intiated: false,
        title: "",
        startDate: null,
        endDate: null,
        project: null,
        department: null,
        participants: null,
        agenda: null,
        location: null,
        documents: null,
      },
    };
    addToStrPrototype();
  }
  componentDidMount() {
    this.processSpeechGuessState(this.props.fullSpeech[0]);
  }
  componentDidUpdate(pp) {
    if (pp.fullSpeech.length !== this.props.fullSpeech.length) {
      this.processSpeechGuessState(this.props.fullSpeech[0]);
    }
  }
  processSpeechGuessState = (speech = "") => {
    const str = speech;
    const array = str.split(" ");
    const idxOfOn = array.lastIndexOf("on");
    const idxOfAt = array.lastIndexOf("at");
    const idxOfFor = array.lastIndexOf("for");
    const idxOfToday = array.lastIndexOf("today");
    const idxOfTomorrow = array.lastIndexOf("tomorrow");
    const idxOfDur = array.lastIndexOf("duration");
    const acc = {};
    if (idxOfFor !== -1) {
      // has for three conditions from here
      if (idxOfOn !== -1) {
        // has on so it must be date specific speech
        acc.title = array.slice(idxOfFor + 1, idxOfOn).join(" ");
        // acc.time = array.slice(idxOfAt + 1, idxOfAt + 3).join(' '); // caculate time using p.m a.m. keyword
        const mIdx = speech.regexLastIndexOf(monthsRegx);
        console.log(speech.indexOf(" ", mIdx));
        console.log(
          "date",
          speech.substring(mIdx - 3, speech.indexOf(" ", mIdx))
        );
        acc.date = new Date(
          array.slice(idxOfOn + 1, idxOfOn + 3).join(" ")
        ).toString();
      } else if (idxOfOn === -1) {
        // there ca be at or not so check for the number and a.m. p.m.
        const timeIdx = array.lastIndexOf(/[1-31]/g);
        // has only at so, it must be having today or tomorrow
        acc.title = array.slice(idxOfFor + 1, idxOfAt).join(" ");
        acc.time = array.slice(idxOfAt + 1, idxOfAt + 3).join(" ");
        if (idxOfToday !== -1) {
          acc.date = new Date().toString();
        } else if (idxOfTomorrow !== -1) {
          const date = new Date();
          date.setDate(date.getDate() + 1);
          acc.date = date.toString();
        }
      } else {
        acc.title = "five words after for";
      }
      if (idxOfDur !== -1) {
        acc.duration = array.slice(idxOfDur + 1, idxOfDur + 3).join(" ");
      }
    }
    console.log(acc);
    console.log("process and set the state", speech);
    // 'create a meeting for project discussion on 28 june at 2:00 p.m. duration 1 hour';
    // create a meeting for project discussion at 12:00 p.m. tomorrow duration 1 hour;
    // create a meeting for what ever the title is and user does not speek the on or at, what to do then?
    // create a meeting for project discussion on 28 june at 2:00 pm duration 1 hour
  };

  handleInputChange = (name) => ({ target: { value } }) => {
    this.setState({
      createMeeting: { ...this.state.createMeeting, [name]: value },
    });
  };
  render() {
    const {
      createMeeting: { title },
    } = this.state;
    return (
      <div className="createMeeting">
        <h2>Create Meeting</h2>
        <Row>
          <Col sm={12}>
            <FormGroup>
              <Input
                onChange={this.handleInputChange("title")}
                type="text"
                id="title"
                value={title}
                placeholder=" "
              />
              <Label for="title" className="mb-0">
                Title
              </Label>
            </FormGroup>
          </Col>
        </Row>
        <Row className={cx({ "overlay-disabled": !title })}>
          <Col sm={12}>
            <div className="mb-4 mt-1 meetings-dropdown">
              <AssistantDropdown />
              <AssistantDropdown />
            </div>
          </Col>
        </Row>
        <StartEndTime className={cx({ "overlay-disabled": !title })} />
        <Button className="right-arrow">
          <img src={ContinueArrow} alt="" />
        </Button>
        {/* <MeetingOptions  /> */}
      </div>
    );
  }
}
export default CreateMeeting;
