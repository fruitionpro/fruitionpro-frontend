import React from "react";
import { Button, Row, Col, Input } from "reactstrap";
import Clip from "../../../../../../asset/images/virtual-assistant/clip.png";
import CrossIcon from "../../../../../icons/cross";

export default () => {
  return (
    <div className="add-documnets">
      <div className="add-file">
        <h4>
          <img src={Clip} alt="attachment" className="mr-2" />
          <span>Add file</span> or drop file here
        </h4>
      </div>
      <div className="file-details-wrapper">
        <div className="detail-list">
          <div className="file-info">
            <p className="mb-0">
              <span className="file-name">
                <span className="title">
                  Screenshot 2020-06-17 at ajfghjsgfs
                </span>
                <span>.png</span>
              </span>
              <span className="file-size">10kb</span>
            </p>
            <CrossIcon />
          </div>
          <div className="process-error"></div>
        </div>
        <div className="detail-list error">
          <div className="file-info">
            <p className="mb-0">
              <span className="file-name">
                <span className="title">Screenshot 2020-06-17</span>
                <span>.png</span>
              </span>
              <span className="file-size">10kb</span>
            </p>
            <CrossIcon />
          </div>
          <div className="process-error">Retry</div>
        </div>
      </div>
    </div>
  );
};
