import React from "react";
import { Row, Col } from "reactstrap";
import { DateTimeCard } from "../../../shared/components";
// import { defaultProps } from "react-select/src/Select";

const Timer = (props) => {
  return (
    <Row className={`text-white ${props.className || ""}`}>
      <Col sm={6} className="pr-1">
        <DateTimeCard type="start" />
      </Col>
      <Col sm={6} className="pl-1">
        <DateTimeCard type="end" />
      </Col>
    </Row>
  );
};
export default Timer;
