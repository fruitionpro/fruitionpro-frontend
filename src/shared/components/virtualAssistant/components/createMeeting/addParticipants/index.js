import React from "react";
import { Button, Row, Col, Input } from "reactstrap";
import UserImg from "../../../../../../asset/images/virtual-assistant/user.jpg";
import TickIcon from "../../../../../../asset/images/virtual-assistant/tick.svg";
// import OvalIcon from "../../../../asset/images/virtual-assistant/oval.svg";

const mockData = [
  { image: UserImg, name: "Alex anderson" },
  { image: UserImg, name: "Alex anderson" },
  { image: UserImg, name: "Alex anderson" },
  { image: UserImg, name: "Alex anderson" },
  { image: UserImg, name: "Alex anderson" },
  { image: UserImg, name: "Alex anderson" },
];

export default () => {
  return (
    <div className="add-participants">
      <div className="recent-users">
        <h5 className="list-title">Recently Added</h5>
        <div className="user-profile">
          {mockData.map(({ image, name }, index) => (
            <img key={index} src={image} className="profile-img" alt={name} />
          ))}
        </div>
      </div>
      <div className="search-wrapper">
        <Input type="search" name="search" placeholder="Search..." />
      </div>
      <div className="recentUserListWrapper">
        {mockData.map(({ image, name }, index) => (
          <div key={index} className="recentUserList">
            <div className="d-flex align-items-center">
              <img src={image} className="authorImg" alt={name} />
              <p className="name mb-0">{name}</p>
            </div>
            <img src={TickIcon} className="" alt="check" />
          </div>
        ))}
      </div>
    </div>
  );
};
