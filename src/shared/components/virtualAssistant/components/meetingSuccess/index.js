import React from "react";
import CheckIcon from "../../../../../asset/images/virtual-assistant/successfully.png";

export default () => {
  return (
    <div className="meeting-success">
      <div>
        <img src={CheckIcon} />
        <h4>Meeting Created Successfully</h4>
      </div>
    </div>
  );
};
