import { LOGOUT_USER, SET_LOGIN_CREDENTIALS } from "./types";
import Modal from '../../../util/index';
import { LOGIN_USER, SOCIAL_LOGIN, SELECT_USER_TYPE, SELECT_ORGANIZATION, UPDATE_USER_PROFILE, UPDATE_ORGANIZATION_PROFILE, REGISTER_USER } from '../../../store/actionTypes/index';
import { composeResetReducer } from '../../../store/reducers/reset-reducer';
import { SET_USER_TYPE } from "../../accountType/store/types";

const initialState = {
  email: null,
  name: null,
  isLoginSuccess: false,
  socialLogin: false,
  socialLoginType: null,
  calender: null,
  userType: null,
  isLicenseExpired: false
}

const LoginReducer = (state = initialState, action) => {
  switch (action.type) {

    case LOGIN_USER.SUCCESS: {
      const { status, message, data } = action.payload.data;
      Modal(status, message)
      if (status === 200) {
        let licenseExpired = null;
        const { name, email, type } = data;
        if (type === 'individual' || type === null) {
          licenseExpired = data && data.isLicenseExpired
        }
        if (type === 'organization') {
          const { organization } = data;
          licenseExpired = organization && organization.isLicenseExpired
        }
        return {
          ...state,
          isLoginSuccess: true,
          name,
          email,
          calender: data && data.preferred_calender ? data.preferred_calender : null,
          isLicenseExpired: licenseExpired
        }

      }
      else {
        return state
      }
    }
    case REGISTER_USER.SUCCESS: {
      const { status, data } = action.payload.data;
      if (status === 200) {
        const { name, email } = data;
        return {
          ...state,
          name,
          email,
        }
      }
      else {
        return state;
      }
    }
    case SELECT_USER_TYPE.SUCCESS: {
      const { status, data } = action.payload.data;
      if (status === 200) {
        return {
          ...state,
          isLoginSuccess: true,
          calender: data && data.preferred_calender ? data.preferred_calender : null
        }
      }
      else {
        return state
      }
    }
    case SELECT_ORGANIZATION.SUCCESS: {
      const { status, data } = action.payload.data;
      if (status === 200) {
        return {
          ...state,
          isLoginSuccess: true,
          calender: (data && data.user && data.user.preferred_calender) ? data.user.preferred_calender : null
        }
      }
      else {
        return state;
      }
    }
    case SOCIAL_LOGIN.SUCCESS: {
      const { status, data, exist, message } = action.payload.data;
      Modal(status, message)
      if (status === 200) {
        if (exist) {
          const { email, name, socialLoginType, isLicenseExpired } = data.user;
          const { organization } = data;
          return {
            ...state,
            isLoginSuccess: true,
            socialLogin: true,
            email: email || null,
            name: name || null,
            socialLoginType,
            calender: data && data.user && data.user.preferred_calender ? data.user.preferred_calender : null,
            isLicenseExpired: (organization && organization.isLicenseExpired) || isLicenseExpired
          }
        }
        else {
          const { socialLoginType } = data;
          return {
            ...state,
            email: data && data.email,
            name: data && data.name,
            socialLogin: true,
            socialLoginType,
            calender: data && data.user && data.preferred_calender ? data.preferred_calender : null

          }
        }
      }
      else {
        return state
      }
    }
    case SET_LOGIN_CREDENTIALS: {
      return {
        ...state,
        email: action && action.data && action.data.email,
        name: action && action.data && action.data.name
      }
    }
    case LOGOUT_USER: {
      return {
        ...state,
        isLoginSuccess: false
      }
    }
    case UPDATE_USER_PROFILE.SUCCESS: {
      const { status, data } = action.payload.data;
      if (status === 200) {
        return {
          ...state,
          name: data && data.name,
          calender: data && data.preferred_calender ? data.preferred_calender : state.calender,
          isLicenseExpired: data && data && data.isLicenseExpired
        }
      }
      else {
        return {
          ...state,
        }
      }

    }
    case UPDATE_ORGANIZATION_PROFILE.SUCCESS: {
      const { status, data } = action.payload.data;
      if (status === 200) {
        return {
          ...state,
          name: data && data.personalDetails && data.personalDetails.name ? data.personalDetails.name : state.name,
          calender: data && data.personalDetails && data.personalDetails.preferred_calender ? data.personalDetails.preferred_calender : state.calender,
          isLicenseExpired: data && data.isLicenseExpired
        }
      }
      else {
        return {
          ...state
        }
      }
    }
    case SET_USER_TYPE: {
      return {
        ...state,
        userType: action.payload
      }
    }
    default: {
      return state
    }
  }
}
export default composeResetReducer(LoginReducer, initialState)
