import { action } from "../../../store/actions/action";
import { IMPORT_SOCIAL_MEETINGS, IMPORT_SOCIAL_CONTACTS, INVITE_USER, SET_MEETING_DATA, SEARCH_MEETINGS } from "../../../store/actionTypes";
import { TOGGLE_PRIVATE_NOTE, DELETE_INVITED_USER, ADD_INVITE_CONTACT, SET_MEETINGS_TYPE } from "./types"
export const importSocialMeetings = (body) => action({
  types: [IMPORT_SOCIAL_MEETINGS.REQUEST, IMPORT_SOCIAL_MEETINGS.SUCCESS],
  payload: {
    request: {
      url: 'meeting/meetings',
      data: body,
      method: 'POST'
    }
  }
});

export const importSocialContacts = (body) => action({
  types: [IMPORT_SOCIAL_CONTACTS.REQUEST, IMPORT_SOCIAL_CONTACTS.SUCCESS],
  payload: {
    request: {
      url: 'meeting/contacts',
      data: body,
      method: 'POST'
    }
  }
})


export const setMeetingData = (data, isCreate) => ({
  type: SET_MEETING_DATA,
  payload: data,
  isCreate
})



export const inviteAndSaveUser = (body) => action({
  types: [INVITE_USER.REQUEST, INVITE_USER.SUCCESS],
  payload: {
    request: {
      url: 'meeting/invite',
      data: body,
      method: 'POST'
    }
  }
})

export const togglePrivateNote = (isOpen) => ({
  type: TOGGLE_PRIVATE_NOTE,
  payload: isOpen
})

export const deleteInvitedUser = (data) => ({
  type: DELETE_INVITED_USER,
  payload: data
})

export const addInviteContact = (data) => ({
  type: ADD_INVITE_CONTACT,
  payload: data
})

export const searchMeetings = (body) => action({
  types: [SEARCH_MEETINGS.REQUEST, SEARCH_MEETINGS.SUCCESS],
  payload: {
    request: {
      url: 'meeting/search',
      data: body,
      method: 'POST'
    }
  }
})

