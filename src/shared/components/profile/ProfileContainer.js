import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ProfileComponent from "./ProfileComponent";
import { updateUserProfile, updateOrganizationProfile, changePassword , getUserOrOrganization} from './store/action';

const mapStateToProps = state => {
  return {
    organizationReducer: state.organizationReducer,
    registerUser: state.registerUser,
    userDetails: state.userDetails,
    individualUserReducer: state.individualUserReducer,
    individualType: state.individualUserReducer.type
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    updateUserProfile,
    updateOrganizationProfile,
    changePassword,
    getUserOrOrganization
  }, dispatch);
};

const ProfileContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileComponent);

export default ProfileContainer;
