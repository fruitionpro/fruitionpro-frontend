import React, { Component } from "react";
import { Button, FormGroup, Label, Input, Row, Col, Modal, ModalHeader, ModalBody } from "reactstrap";
import Select from 'react-select';
import DatePicker from "react-datepicker";
import { Link } from "react-router-dom";
import KeyIcon from '../../../asset/images/icons/Key.png';
import UploadIcon from '../../../asset/images/icons/Upload.png';
import { onFileUpload } from "../../util/fileUpload";
import AddIcon from "../../../asset/images/icons/PlusIcon.png";
import TickIcon from "../../../asset/images/icons/Tick.png";
import CloseIcon from "../../../asset/images/icons/close.svg";
import moment from 'moment';
import Loader from "react-loader-spinner";
import ResetPasswordModal from "../modals/resetPasswordModal";
import SubscriptionPlansModal from "../subscription/subscriptionPlansModal";
import LicenseExpireModal from "./licenseExpireModal";
import queryString from "query-string";
import { withRouter } from "react-router-dom";

class ProfileComponent extends Component {
  constructor(props) {
    super(props);
    const { organizationName, organizationLicenceTimeLeft, organizationLicense, organizationEmail, organizationDepartments, organizationDesignations, organizationLocations, organizationProjects, organizationLogo, organizationExpireDate,
      userDepartment, userDesignation } = this.props.organizationReducer;
    const { email, name, calender } = this.props.userDetails;
    const { license, department, designation, logo, licenseExpireDate, licenseTimeLeft } = this.props.individualUserReducer;
    this.state = {
      isUploading: false,
      file: organizationLogo ? organizationLogo : logo,
      name: organizationName ? organizationName : name,
      license: organizationLicense ? organizationLicense : license,
      email: organizationEmail ? organizationEmail : email,
      department: department ? department : '',
      designation: designation ? designation : '',
      // accExpireDate: organizationEmail ? '' : accExpireDate,
      LicenseExpireDate: organizationExpireDate ? organizationExpireDate : licenseExpireDate ? licenseExpireDate : "",
      LicenseTimeLeft: organizationLicenceTimeLeft ? organizationLicenceTimeLeft : licenseTimeLeft ? licenseTimeLeft : "",
      error: {},
      isDepartmentField: false,
      isDesignationField: false,
      isLocationField: false,
      isProjectField: false,
      departmentOptions: organizationDepartments ? organizationDepartments : [],
      designationOptions: organizationDesignations ? organizationDesignations : [],
      locationOptions: organizationLocations ? organizationLocations : [],
      projectOptions: organizationProjects ? organizationProjects : [],
      departmentOption: {
        value: '',
        label: ''
      },
      designationOption: {
        value: '',
        label: ''
      },
      projectOption: {
        value: '',
        label: ''
      },
      locationOption: {
        value: '',
        label: ''
      },
      modal: false,
      userName: name ? name : '',
      userEmail: email ? email : "",
      userDepartment: {
        label: userDepartment ? userDepartment : '',
        value: userDepartment ? userDepartment : ''
      },
      userDesignation: {
        label: userDesignation ? userDesignation : '',
        value: userDesignation ? userDesignation : ''
      },
      calenderOption: {
        label: calender ? calender : '',
        value: calender ? calender : ''
      },
      isSubscriptionModal: false,
      isExpireModalOpen: true
    };
    this.toggle = this.toggle.bind(this);

  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    })
  }
  validateEmail = email => {
    let re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  };

  licenseTime = data => {
    let time = "";
    if (data) {
      if (data.days > 0) {
        if (data.days === 1) time = time.concat(data.days + " day");
        else time = time.concat(data.days + " days");
      }
      if (data.hours > 0) {
        if (data.hours === 1) time = time.concat(" " + data.hours + " hour");
        else time = time.concat(" " + data.hours + " hours");
      }
      if (data.minutes > 0) {
        if (data.minutes === 1) time = time.concat(" " + data.minutes + " minute");
        else time = time.concat(" " + data.minutes + " minutes");
      }
    }
    if (time) {
      time = time.concat(" left");
    }
    return time;
  }

  onHandelChange = (e, input) => {
    if (input === "department" || input === "designation") {
      this.setState({
        [input]: e.value
      })
    }
    else
      this.setState({
        [e.target.name]: e.target.value
      })
  }
  sendRequest = (e, type) => {
    e.preventDefault();
    const {
      name,
      department,
      designation,
      email,
      license,
      accExpireDate,
      file,
      departmentOption,
      designationOption,
      calenderOption
    } = this.state;
    // set error
    let error = {
      name: false,
      department: false,
      designation: false,
      email: false,
      license: false,
      accExpireDate: false,
      departmentOption: false,
      designationOption: false
    }

    if (name === "") {
      error.name = true;
    }

    if (type === 'organization') {
      if (departmentOption === '' || departmentOption === null) {
        error.departmentOption = true
      }
      if (designationOption === '' || designationOption === null) {
        error.designationOption = true
      }
    }
    if (email === "") {
      error.email = true;
    }
    if (email !== "") {
      if (!this.validateEmail(email)) {
        error.email = true
      }
    }
    if (license === "") {
      error.license = true;
    }


    this.setState({ error });
    if (!error.email && !error.name && !error.license && !error.department && !error.designation && !error.accExpireDate) {
      if (type === 'user') {
        const { updateUserProfile, individualUserReducer, organizationReducer } = this.props;
        const { userId } = this.props.registerUser;
        let licenseType = null;
        if (individualUserReducer.type) {
          licenseType = individualUserReducer.type
        }
        if (organizationReducer.type) {
          licenseType = organizationReducer.type
        }
        if (!individualUserReducer.type && !organizationReducer.type) {
          licenseType = 'invitedUser'
        }
        let inviteObj = {
          name,
          email,
          license,
          licenseType,
          department,
          designation,
          accExpireDate,
          id: userId,
          logo: file,
          preferred_calender: calenderOption.value,
        }
        updateUserProfile(inviteObj).then(result => {
          const { status, data } = result.payload.data;
          if (status === 200) {
            this.setState({
              error: {},
              LicenseTimeLeft: data && data.leftTimeToExpireLicense,
              LicenseExpireDate: data && data.license_key && data.license_key.expiry_date
            })
          }
        })
      }
      else if (type === 'organization') {
        const { updateOrganizationProfile, organizationReducer, registerUser } = this.props;
        const { departmentOptions, designationOptions, projectOptions, locationOptions, userDepartment, userDesignation, userEmail, userName } = this.state;
        const obj = {
          id: organizationReducer.organizationId,
          name,
          email,
          license,
          departmentOptions,
          designationOptions,
          projectOptions,
          locationOptions,
          userEmail,
          userName,
          userDepartment,
          userDesignation,
          preferred_calender: calenderOption.value,
          logo: file,
          userId: registerUser.userId
        }
        updateOrganizationProfile(obj).then(result => {
          const { status, data } = result.payload.data;
          if (status === 200) {
            this.setState({
              LicenseTimeLeft: data && data.leftTimeToExpireLicense,
              LicenseExpireDate: data && data.license_key && data.license_key.expiry_date
            })
          }
        })
      }
    }
  }
  handleFileUpload = (e) => {
    this.setState({
      isUploading: true
    })
    onFileUpload(e.target.files[0]).then((res) => {
      if (res) {
        this.setState({ file: res, isUploading: false })
      }
    })
  }

  componentDidMount() {
    const { getUserOrOrganization, organizationReducer, registerUser, userDetails, location, history } = this.props;
    const userId = registerUser && registerUser.userId;
    if (location && location.search) {
      const values = queryString.parse(location.search);
      const paramUserId = values && values.uid;
      if (paramUserId === userId) {
        this.toggleSubscriptionModal();
      }
    }
    let obj;
    if (organizationReducer.type === 'organization') {
      obj = {
        id: organizationReducer.organizationId,
        type: 'organization'
      }
    } else {
      obj = {
        id: registerUser && registerUser.userId,
        type: 'user'
      }
    }

    getUserOrOrganization(obj).then(result => {
      if (result.payload.status === 200) {
        const { details } = result.payload.data;
        this.setState({
          name: details && details.name ? details.name : "",
          license: details && details.license_key && details.license_key.key ? details.license_key.key : "",
          email: details && details.email ? details.email : "",
          department: details && details.department ? details.department : "",
          designation: details && details.designation ? details.designation : "",
          LicenseTimeLeft: details && details.leftTimeToExpireLicense ? details.leftTimeToExpireLicense : "",
          LicenseExpireDate: details && details.license_expire_date ? details.license_expire_date : "",
        });
      }
    })
  }

  selectHandler = () => {
    this.setState({ designation2: true })
  }
  onChangeHandle = (e, type) => {
    this.setState({ [`${type}Option`]: { label: e.target.value, value: e.target.value } })
  }
  handleClick = (e, type) => {
    e.preventDefault();
    const departmentOption = {
      value: '',
      label: ''
    }
    const designationOption = {
      value: '',
      label: ''
    }
    const projectOption = {
      value: '',
      label: ''
    }
    const locationOption = {
      value: '',
      label: ''
    }

    if (type === 'department') {
      this.setState({ isDepartmentField: !this.state.isDepartmentField, departmentOption })
    }
    if (type === 'designation') {
      this.setState({ isDesignationField: !this.state.isDesignationField, designationOption })
    }
    if (type === 'project') {
      this.setState({ isProjectField: !this.state.isProjectField, projectOption })
    }
    if (type === 'location') {
      this.setState({ isLocationField: !this.state.isLocationField, locationOption })
    }

  }

  addOptionInSelect = (e, type) => {

    const newOption = this.state[`${type}Option`];
    if (newOption.value) {
      let previousOptions = this.state[`${type}Options`] && this.state[`${type}Options`];
      let nextOptions = previousOptions && previousOptions.length > 0 ? previousOptions.slice() : []
      const index = nextOptions.findIndex(data => data.value === newOption.value)
      if (index < 0) {
        nextOptions.push(newOption)
        this.setState({ [`${type}Options`]: nextOptions })
      }
    }
    this.handleClick(e, type)
  }

  onDeleteOption = (option, type) => {

    const { departmentOptions, designationOptions, locationOptions, projectOptions } = this.state;
    if (type === 'department') {
      let index = departmentOptions.findIndex(element => element.value === option.value);
      departmentOptions.splice(index, 1);
      this.setState({
        departmentOption: {
          value: '',
          label: ''
        }
      })
    }
    if (type === 'designation') {
      let index = designationOptions.findIndex(element => element.value === option.value);
      designationOptions.splice(index, 1);
      this.setState({
        designationOption: {
          value: '',
          label: ''
        }
      })
    }
    if (type === 'project') {
      let index = projectOptions.findIndex(element => element.value === option.value);
      projectOptions.splice(index, 1);
      this.setState({
        projectOption: {
          value: '',
          label: ''
        }
      })
    }
    if (type === 'location') {
      let index = locationOptions.findIndex(element => element.value === option.value);
      locationOptions.splice(index, 1);
      this.setState({
        locationOption: {
          value: '',
          label: ''
        }
      })
    }

  }
  getFormatedOptions = (data, type) => {
    if (data && data.length > 0) {
      let options = []
      data.forEach(element => {
        options.push({
          label: <p>{element.label}<span onClick={(e) => {
            e.stopPropagation();
            this.onDeleteOption(element, type)
          }}><img src={CloseIcon} /></span></p>, value: element.value
        })
      })
      return options
    }
    return []
  }

  toggleSubscriptionModal = () => {
    // e.preventDefault();
    this.setState({
      isSubscriptionModal: !this.state.isSubscriptionModal
    })
  }
  toggleExpireModal = () => {
    this.setState({
      isExpireModalOpen: !this.state.isExpireModalOpen
    })
  }
  render() {

    const { file, LicenseExpireDate, LicenseTimeLeft, name, email, license, error, isDepartmentField, isUploading, calenderOption,
      departmentOption, designationOption, isDesignationField, designationOptions, departmentOptions, projectOptions, locationOptions, locationOption, projectOption, isLocationField, isProjectField, userName, userEmail, userDepartmentOptions,
      userDepartment, userDesignation, isSubscriptionModal, isExpireModalOpen } = this.state;
    const { type } = this.props.organizationReducer;
    // const { licenseExpireDate, licenseTimeLeft } = this.props.individualUserReducer;
    const { individualUserReducer, individualType, changePassword, userDetails } = this.props;
    const { userId } = this.props.registerUser;
    const calenderOptions = [
      { label: 'Google', value: 'Google' },
      { label: 'Outlook', value: 'Outlook' }

    ]
    if (userDetails.isLicenseExpired && isExpireModalOpen) {
      return < LicenseExpireModal isOpen={isExpireModalOpen} toggle={this.toggleExpireModal} />
    }

    return (
      <div className="dashboard-wrapper">
        <div className="dashboard-content">
          {
            type === 'organization' ?
              <>
                <div className="dashboard-title"><h2>Organization Profile</h2></div>
                <div className="profile-wrap">
                  <div className="account-form-screen">
                    <div className="text-center upload-icon">
                      {isUploading ?
                        <div className="doc-content">
                          <span style={{ textAlign: "center" }}>
                            <Loader
                              type="TailSpin"
                              color="#00BFFF"
                              height={60}
                              width={60}
                            />
                            Uploading...
                        </span>
                        </div>
                        :
                        <div>
                          <input accept="image/x-png,image/gif,image/jpeg" type="file" className="d-none" id="f-upload" onChange={this.handleFileUpload} />
                          <label htmlFor="f-upload" >
                            <div className="camera-icon">
                              <img className="d-block" src={file && file ? file : UploadIcon} style={{
                                width: 100, height: 100, borderRadius: '100%',
                                objectFit: 'contain'
                              }} alt="UploadIcon" />
                            </div>
                          </label>
                        </div>
                      }
                    </div>
                    <Row>
                      <Col sm={12}>
                        <h3>Company Details</h3>
                      </Col>
                      <Col sm={12}>
                        <FormGroup>
                          <Label htmlFor="" className="required">Company Name</Label>
                          <Input type="text" name="name" value={name} onChange={(e) => this.onHandelChange(e)} placeholder="Enter Company Name" />
                          {error.name && <span className="text-danger">{"Name is empty"}</span>}
                        </FormGroup>
                      </Col>

                      <Col sm={6}>
                        <FormGroup>
                          <Label htmlFor="">Department</Label>
                          <div className="selectContent">
                            {isDepartmentField ?
                              <Input type="text" autoFocus value={departmentOption.value || ""}
                                onBlur={(e) => this.addOptionInSelect(e, "department")}
                                onChange={(e) => this.onChangeHandle(e, "department")}
                                name="department" placeholder="Enter Department" />
                              :
                              <Select
                                // defaultMenuIsOpen={true}
                                value={departmentOption.value ? { label: departmentOption.value, value: departmentOption.value } : { value: '', label: "Add New" }}
                                className="popup-select"
                                classNamePrefix="profile"
                                onChange={(data) => {
                                  this.setState({ departmentOption: data })
                                }}
                                name="department"
                                options={this.getFormatedOptions(departmentOptions, 'department')}
                              />
                            }
                            {isDepartmentField ? <Button onClick={(e) => this.addOptionInSelect(e, 'department')}> <img src={TickIcon} alt="tickImg" /> </Button> :
                              <Button onClick={(e) => this.handleClick(e, 'department')}><img src={AddIcon} alt="addIcon" /> </Button>}
                          </div>
                          {error.departmentOption && <span className="text-danger">Please select department</span>}
                        </FormGroup>
                      </Col>
                      <Col sm={6}>
                        <FormGroup>
                          <Label htmlFor="">Designation</Label>
                          <div className="selectContent">
                            {
                              isDesignationField ?
                                <Input type="text" autoFocus value={designationOption.value || ""}
                                  onBlur={(e) => this.addOptionInSelect(e, "designation")}
                                  onChange={(e) => this.onChangeHandle(e, "designation")} name="designation" placeholder="Enter Designation" />
                                :
                                <Select
                                  // defaultMenuIsOpen={true}
                                  value={designationOption.value ? { label: designationOption.value, value: designationOption.value } : { value: '', label: "Add New" }}
                                  className="popup-select"
                                  classNamePrefix="profile"
                                  onChange={(data) => {
                                    this.setState({ designationOption: data })
                                  }}
                                  name="designation"
                                  options={this.getFormatedOptions(designationOptions, 'designation')}
                                />
                            }
                            {isDesignationField ? <Button onClick={(e) => this.addOptionInSelect(e, 'designation')}> <img src={TickIcon} alt="tickImg" /> </Button> :
                              <Button onClick={(e) => this.handleClick(e, 'designation')}><img src={AddIcon} alt="addIcon" /> </Button>}
                          </div>
                          {error.designationOption && <span className="text-danger">Please select designation</span>}
                        </FormGroup>
                      </Col>

                      <Col sm={6}>
                        <FormGroup>
                          <Label htmlFor="">Projects</Label>
                          <div className="selectContent">
                            {isProjectField ?
                              <Input type="text" autoFocus value={projectOption.value || ""}
                                onBlur={(e) => this.addOptionInSelect(e, "project")}
                                onChange={(e) => this.onChangeHandle(e, "project")}
                                name="project" placeholder="Enter Project" />
                              :
                              <Select
                                // defaultMenuIsOpen={true}
                                value={projectOption.value ? { label: projectOption.value, value: projectOption.value } : { value: '', label: "Add New" }}
                                className="popup-select"
                                classNamePrefix="profile"
                                onChange={(data) => {
                                  this.setState({ projectOption: data })
                                }}
                                name="project"
                                options={this.getFormatedOptions(projectOptions, 'project')}
                              />
                            }
                            {isProjectField ? <Button onClick={(e) => this.addOptionInSelect(e, 'project')}> <img src={TickIcon} alt="tickImg" /> </Button> :
                              <Button onClick={(e) => this.handleClick(e, 'project')}><img src={AddIcon} alt="addIcon" /> </Button>}
                          </div>
                          {error.projectOption && <span className="text-danger">Please select project</span>}
                        </FormGroup>
                      </Col>
                      <Col sm={6}>
                        <FormGroup>
                          <Label htmlFor="">Location</Label>
                          <div className="selectContent">
                            {
                              isLocationField ?
                                <Input type="text" autoFocus value={locationOption.value || ""}
                                  onBlur={(e) => this.addOptionInSelect(e, "location")}
                                  onChange={(e) => this.onChangeHandle(e, "location")} name="location" placeholder="Enter location" />
                                :
                                <Select
                                  // defaultMenuIsOpen={true}
                                  value={locationOption.value ? { label: locationOption.value, value: locationOption.value } : { value: '', label: "Add New" }}
                                  className="popup-select"
                                  classNamePrefix="profile"
                                  onChange={(data) => {
                                    this.setState({ locationOption: data })
                                  }}
                                  name="location"
                                  options={this.getFormatedOptions(locationOptions, 'location')}
                                />
                            }
                            {isLocationField ? <Button onClick={(e) => this.addOptionInSelect(e, 'location')}> <img src={TickIcon} alt="tickImg" /> </Button> :
                              <Button onClick={(e) => this.handleClick(e, 'location')}><img src={AddIcon} alt="addIcon" /> </Button>}
                          </div>
                          {error.locationOption && <span className="text-danger">Please select location</span>}
                        </FormGroup>
                      </Col>


                      <Col sm={12}>
                        <FormGroup>
                          <Label htmlFor="" className="required">License Key</Label>
                          <div className="key-wrap">
                            <Input type="text" name="license" value={license} onChange={(e) => this.onHandelChange(e)} placeholder="Enter license key" className="license-key" />
                            <img src={KeyIcon} alt="keyIcon" className="key" />
                          </div>
                          {error.license && <span className="text-danger">{"Invalid license"}</span>}
                        </FormGroup>
                      </Col>
                      <Col sm={12}>
                        <FormGroup>
                          <Label htmlFor="">Account expires on</Label>
                          <div className="profile-date">
                            <DatePicker
                              selected={LicenseExpireDate ? moment(LicenseExpireDate).toDate() : null}
                              disabled
                              className="form-control date"
                              dateFormat="MM/dd/yyyy"
                              minDate={moment().toDate()}
                            // dateFormat="yyyy/MM/dd"

                            />
                            {/* {LicenseTimeLeft && LicenseTimeLeft.days && LicenseTimeLeft.days > 0 ? <span>{LicenseTimeLeft.days} days</span> : ""}{LicenseTimeLeft && LicenseTimeLeft.hours && LicenseTimeLeft.hours > 0 ? <span> {LicenseTimeLeft.hours} hours left</span> : ""} */}
                            <span>{this.licenseTime(LicenseTimeLeft)}</span>
                          </div>
                          {/* {error.accExpireDate && <span className="text-danger">{"Please select expiry date"}</span>} */}
                        </FormGroup>
                      </Col>
                      <Col sm={12}>
                        <div><p className="bottom-text text-left mb-0">Extend License?<Link onClick={(e) => this.toggleSubscriptionModal(e)}  >Click here</Link></p></div>
                      </Col>
                    </Row>
                    <hr />
                    <Row className="mb-5">
                      <Col sm={12}>
                        <h3>Personal Details</h3>
                      </Col>
                      <Col sm={12}>
                        <FormGroup>
                          <Label htmlFor="" className="required">Name</Label>
                          <Input type="text" name="userName" value={userName} onChange={(e) => this.onHandelChange(e)} placeholder="Enter your name" />
                        </FormGroup>
                      </Col>
                      <Col sm={6}>
                        <FormGroup>
                          <Label htmlFor="">Department</Label>
                          <Select
                            // defaultMenuIsOpen={true}
                            value={userDepartment.value ? { label: userDepartment.value, value: userDepartment.value } : { value: '', label: "Select" }}
                            className="popup-select"
                            classNamePrefix="userDepartment"
                            onChange={(data) => {
                              this.setState({ userDepartment: data })
                            }}
                            name="userDepartment"
                            options={departmentOptions}
                          />
                        </FormGroup>
                      </Col>
                      <Col sm={6}>
                        <FormGroup>
                          <Label htmlFor="">Designation</Label>
                          <Select
                            value={userDesignation.value ? { label: userDesignation.value, value: userDesignation.value } : { value: '', label: "Select" }}
                            className="popup-select"
                            classNamePrefix="userDesignation"
                            onChange={(data) => {
                              this.setState({ userDesignation: data })
                            }}
                            name="userDesignation"
                            options={designationOptions}
                          />
                        </FormGroup>
                      </Col>
                      <Col sm={12}>
                        <FormGroup>
                          <Label htmlFor="" className="required">Email </Label>
                          <Input type="email" readOnly value={userEmail} onChange={(e) => this.onHandelChange(e)} name="userEmail" placeholder="Enter Email" />
                          {error.email && <span className="text-danger">{"Please select email"}</span>}
                        </FormGroup>
                      </Col>
                      <Col sm={12}>
                        <FormGroup>
                          <Label htmlFor="">Preferred Calendar</Label>
                          <Select
                            value={calenderOption.value ? { label: calenderOption.value, value: calenderOption.value } : { value: '', label: "Select" }}
                            className="popup-select"
                            classNamePrefix="calenderOption"
                            onChange={(data) => {
                              this.setState({ calenderOption: data })
                            }}
                            name="calenderOption"
                            options={calenderOptions}
                          />
                        </FormGroup>
                      </Col>
                      <Col sm={12}>
                        <center><Button color="gradient" className="profile-btn" onClick={(e) => this.sendRequest(e, "organization")} >Save</Button></center>
                      </Col>
                      <Col sm={12}>
                        <div><span onClick={this.toggle} className="resetPass">Reset Password</span></div>
                      </Col>
                    </Row>

                  </div>
                </div>
              </>
              :
              <>
                <div className="dashboard-title"><h2>User Profile</h2></div>
                <div className="profile-wrap">
                  <div className="account-form-screen">
                    <div className="text-center upload-icon">
                      {isUploading ?
                        <div className="doc-content">
                          <span style={{ textAlign: "center" }}>
                            <Loader
                              type="TailSpin"
                              color="#00BFFF"
                              height={60}
                              width={60}
                            />
                            Uploading...
                        </span>
                        </div>
                        :
                        <div>
                          <input type="file" accept="image/x-png,image/gif,image/jpeg" className="d-none" id="f-upload" onChange={this.handleFileUpload} />
                          <label htmlFor="f-upload" ><div className="camera-icon">
                            <img className="d-block" src={file && file ? file : UploadIcon} style={{
                              width: 100, height: 100, borderRadius: '100%',
                              objectFit: 'cover'
                            }} alt="UploadIcon" /></div>
                          </label>
                        </div>
                      }
                    </div>
                    <Row>
                      <Col sm={6}>
                        <FormGroup>
                          <Label htmlFor="" className="required">Name</Label>
                          <Input type="text" name="name" value={name} onChange={(e) => this.onHandelChange(e)} placeholder="Enter First Name" />
                          {error.name && <span className="text-danger">{"Name is empty"}</span>}
                        </FormGroup>
                      </Col>
                      <Col sm={6}>
                        <FormGroup>
                          <Label htmlFor="" className="required">Email </Label>
                          <Input type="email" readOnly value={email} onChange={(e) => this.onHandelChange(e)} name="email" placeholder="Enter Email" />
                          {error.email && <span className="text-danger">{"Please select email"}</span>}
                        </FormGroup>
                      </Col>
                      {(individualType === 'invitedUser' || individualType === null) &&
                        <Col sm={12}>
                          <Row>
                            <Col sm={6}>
                              <FormGroup>
                                <Label htmlFor="">Department</Label>
                                <Input type="text" readOnly value={individualUserReducer.department} onChange={(e) => this.onHandelChange(e)} name="department" />
                                {/* name="department" placeholder="Please select department" /> */}
                                {/* {error.department && <span className="text-danger">{"Please select department"}</span>} */}
                              </FormGroup>
                            </Col>
                            <Col sm={6}>
                              <FormGroup>
                                <Label htmlFor="">Designation</Label>
                                <Input type="text" readOnly value={individualUserReducer.designation} onChange={(e) => this.onHandelChange(e)} name="designation" />
                                {/* } name="designation" placeholder="Please select designation" /> */}
                                {/* {error.designation && <span className="text-danger">{"Please select designation"}</span>} */}
                              </FormGroup>
                            </Col>
                          </Row>
                        </Col>}
                      <Col sm={12}>
                        <FormGroup>
                          <Label htmlFor="">Preferred Calendar</Label>
                          <Select
                            value={calenderOption.value ? { label: calenderOption.value, value: calenderOption.value } : { value: '', label: "Select" }}
                            className="popup-select"
                            classNamePrefix="calenderOption"
                            onChange={(data) => {
                              this.setState({ calenderOption: data })
                            }}
                            name="calenderOption"
                            options={calenderOptions}
                          />
                        </FormGroup>
                      </Col>
                      {individualType === 'individual' && <Col sm={12}>
                        <FormGroup>
                          <Label htmlFor="" className="required">License Key</Label>
                          <div className="key-wrap">
                            <Input type="text" name="license" value={license} onChange={(e) => this.onHandelChange(e)} placeholder="Enter license key" className="license-key" />
                            <img src={KeyIcon} alt="keyIcon" className="key" />
                          </div>
                          {error.license && <span className="text-danger">{"Invalid license"}</span>}
                        </FormGroup>
                      </Col>}
                      <Col sm={12}>
                        <FormGroup>
                          <Label htmlFor="">Account expires on</Label>
                          <div className="profile-date">
                            <DatePicker
                              selected={LicenseExpireDate ? moment(LicenseExpireDate).toDate() : null}
                              disabled
                              className="form-control date"
                              dateFormat="MM/dd/yyyy"
                              minDate={moment().toDate()}
                            />
                            {/* {LicenseTimeLeft && LicenseTimeLeft.days && LicenseTimeLeft.days > 0 ? <span>{LicenseTimeLeft.days} days</span> : ""}{LicenseTimeLeft && LicenseTimeLeft.hours && LicenseTimeLeft.hours > 0 ? <span> {LicenseTimeLeft.hours} hours left</span> : ""} */}
                            <span>{this.licenseTime(LicenseTimeLeft)}</span>
                          </div>
                        </FormGroup>
                      </Col>
                      <Col sm={12}>
                        <div><p className="bottom-text text-left mb-0">Extend License?<Link onClick={(e) => this.toggleSubscriptionModal(e)} >Click here</Link></p></div>
                        <div><span onClick={this.toggle} className="resetPass">Reset Password</span></div>

                      </Col>
                      <Col sm={12}>
                        <center><Button color="gradient" className="profile-btn" onClick={(e) => this.sendRequest(e, "user")}>Save</Button></center>
                      </Col>
                    </Row>
                  </div>
                </div>
              </>
          }
        </div>
        {isSubscriptionModal && <SubscriptionPlansModal isOpen={isSubscriptionModal} isToggle={this.toggleSubscriptionModal} />}
        <ResetPasswordModal open={this.state.modal} toggle={this.toggle} changePassword={changePassword} userId={userId} userDetails={userDetails} />
      </div >
    );
  }
}

export default withRouter(ProfileComponent);
