import React, { Component } from "react";
// import RedCloseIcon from "../../../../asset/images/mom-icons/red-close.svg";
import {
  Label,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Input,
  FormGroup,
  Button,
  Form,
  Row,
  Col
} from "reactstrap";
import PDFIcon from "../../../../asset/images/icons/PDF_icon.png";
import Edit from "../../../icons/moreIcon";
import PlusIcon from "../../../../asset/images/icons/PlusIcon.png";
// import AddButtonIcon from "../../../icons/addIcon";
import Select from "react-select";
// import DatePicker from "react-datepicker";
import moment from "moment";
import Loader from "react-loader-spinner";
import { onFileUpload } from "../../../util/fileUpload";
import MsgModal from "../../../util/index";
import MoreIcon from "../../../icons/moreIcon";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { bindActionCreators } from "redux";
import { circleCharacter } from "../../createMeeting/createMeetingComponent";

class SidebarContent extends Component {
  constructor(props) {
    super(props);
    const { taskData } = props;
    this.state = {
      dropdownOpen: false,
      openDropdownValue: "",
      isOpenDeleteMode: false,
      startDate: taskData && taskData.start_date && taskData.start_date,
      endDate: taskData && taskData.end_date && taskData.end_date,
      dueDate: taskData && taskData.due_date && taskData.due_date,
      dropdownDeleteIndex: "",
      dropdownUserIndex: "",
      taskOption: {
        label:
          props.taskData && props.taskData.status ? props.taskData.status : "",
        value:
          props.taskData && props.taskData.status ? props.taskData.status : ""
      },
      isUploading: false,
      taskDocuments:
        props.taskData &&
          props.taskData.documents &&
          props.taskData.documents.length > 0
          ? props.taskData.documents
          : [],
      isDropdownOpen: false,
      isUpdated: false,
      isSaved: false,
      comment: "",
      enterText: ""
    };
    this.deleteDropdown = this.deleteDropdown.bind(this);
  }

  dtToggle = (e, index) => {
    e.preventDefault();
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
      dropdownUserIndex: index,
      id: ""
    });
  };
  onSaveTask = e => {
    e.preventDefault();
    const { taskOption, taskDocuments, startDate } = this.state;
    const {
      updateTask,
      sidebarToggle,
      isUpdated,
      taskData,
      userDetails,
      isFilterApplied
    } = this.props;

    let StartDate = "";
    let EndDate = "";
    if (taskOption.value === "In Progress") {
      StartDate = new Date().toUTCString();
    }
    if (taskOption.value === "Completed") {
      StartDate = startDate;
      EndDate = new Date().toUTCString();
    }

    const obj = {
      start_date: StartDate,
      end_date: EndDate,
      status: taskOption.value,
      id: taskData && taskData._id,
      documents: taskDocuments,
      comments: taskData.comments,
      email: userDetails.email,
      meeting_id: taskData.meeting_id
    };
    updateTask(obj).then(result => {
      const { status } = result.payload.data;
      if (status === 200) {
        sidebarToggle();
        isFilterApplied(false);
        isUpdated(false);
      }
    });
  };

  handleDelete = document => {
    const { taskDocuments } = this.state;
    let oldDocuments = [...taskDocuments];
    const deleteIndex = oldDocuments.findIndex(
      item => item.url === document.url
    );
    if (deleteIndex > -1) {
      oldDocuments.splice(deleteIndex, 1);
      this.setState({
        taskDocuments: oldDocuments,
        openDropdownValue: "",
        isOpenDeleteMode: false
      });
    }
  };

  handleAddTaskDocument = e => {
    const { taskDocuments } = this.state;
    const fileName = e.target.files[0].name;
    let taskDocIndex = -1;
    if (taskDocuments && taskDocuments.length > 0) {
      taskDocIndex = taskDocuments.findIndex(doc => doc.name === fileName);
    }
    if (taskDocIndex < 0) {
      this.setState({
        isUploading: true
      });
      onFileUpload(e.target.files[0]).then(res => {
        if (res) {
          const { taskDocuments } = this.state;
          const newData = [...taskDocuments];
          newData.push({ url: res, name: fileName });
          this.setState({ taskDocuments: newData, isUploading: false });
        }
      });
    } else {
      MsgModal(400, "Document already added");
    }
  };

  toggle(value) {
    this.setState(prevState => ({
      isOpenDeleteMode: !prevState.isOpenDeleteMode,
      openDropdownValue: value
    }));
  }
  deleteDropdown() {
    this.setState({
      isDropdownOpen: !this.state.isDropdownOpen
    });
  }

  onInputChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onFormSubmit = e => {
    e.preventDefault();
    const { email } = this.props.userDetails;
    const { addComment } = this.props;
    this.setState(
      { enterText: this.state.comment, comment: "" },
      this.scrollCommentContentWrapper
    );
    const obj = {
      email,
      message: this.state.comment
    };
    if (this.state.comment) {
      addComment(obj);
    }
  };
  scrollCommentContentWrapper = () => {
    setTimeout(() => {
      let commentContent = document.getElementById("commentContent")
        .clientHeight;
      document.getElementById(
        "commentContentWrapper"
      ).scrollTop = commentContent;
    }, 50);
  };
  deleteTask = (e, data) => {
    e.preventDefault();
    const {
      deleteTask,
      isUpdated,
      sidebarToggle,
      isFilterApplied,
      taskData
    } = this.props;
    const obj = {
      id: data._id,
      admin_email: taskData && taskData.admin_email
    };
    deleteTask(obj).then(result => {
      const { status, message } = result.payload.data;
      MsgModal(status, message);
      if (status === 200) {
        isUpdated(false);
        sidebarToggle();
        isFilterApplied(false);
      }
    });
  };

  render() {
    const {
      startDate,
      endDate,
      taskOption,
      dueDate,
      isUploading,
      openDropdownValue,
      isOpenDeleteMode,
      taskDocuments,
      comment,
    } = this.state;
    const {
      sidebarToggle,
      taskData,
      updateTaskStatus,
      userDetails,
    } = this.props;

    let UtcDueDate = dueDate && moment.utc(dueDate).toDate();
    let DueDate =
      UtcDueDate &&
      moment(UtcDueDate)
        .local()
        .format("YYYY-MM-DD");
    let UtcStartDate = startDate && moment.utc(startDate).toDate();
    let StartDate =
      UtcStartDate &&
      moment(UtcStartDate)
        .local()
        .format("YYYY-MM-DD");
    let UtcEndDate = endDate && moment.utc(endDate).toDate();
    let EndDate =
      UtcEndDate &&
      moment(UtcEndDate)
        .local()
        .format("YYYY-MM-DD");
    const taskOptions = [
      { value: "Not Started", label: "Not Started" },
      { value: "In Progress", label: "In Progress" },
      { value: "Completed", label: "Completed" },
      { value: "Postponed", label: "Postponed" },
      { value: "Cancelled", label: "Cancelled" }
    ];
    return (
      <div className="task-sidebarCard add-agenda-screen">
        <div className="cross" onClick={sidebarToggle}>
          <i className="fas fa-times"></i>
        </div>
        <div className="taskCard-content">
          <div className="tasksHeader">
            <h2>{taskData.task || ""}</h2>
            <div className="user-edit">
              {userDetails.email === taskData.admin_email && (
                <Dropdown
                  isOpen={this.state.isDropdownOpen}
                  toggle={this.deleteDropdown}
                >
                  <DropdownToggle>
                    <span onClick={this.deleteDropdown}>
                      <MoreIcon />
                    </span>
                  </DropdownToggle>
                  <DropdownMenu right>
                    <DropdownItem onClick={e => this.deleteTask(e, taskData)}>
                      Delete
                    </DropdownItem>
                  </DropdownMenu>
                </Dropdown>
              )}
            </div>
          </div>
          <div className="nameContent">
            {taskData && taskData.department && (
              <h3>
                <label className="mb-0">Department :</label>{" "}
                <span>{taskData.department || ""}</span>
              </h3>
            )}
            {taskData && taskData.project_name && (
              <h3>
                <label className="mb-0">Project :</label>
                <span> {taskData.project_name || ""}</span>
              </h3>
            )}
          </div>
          <div className="commentWrapper" id="commentContentWrapper">
            <div id="commentContent">
              {taskData &&
                taskData.comments &&
                taskData.comments.length > 0 &&
                taskData.comments.map(data => {
                  return (
                    <div
                      className={`commentWrap ${
                        data.logo ? "logo" : "only-name"
                        }`}
                    >
                      <label className="image" title={data.email || ""}>
                        <div className="commentUserImg">
                          {/* <span>{data.email && data.email.charAt(0).toUpperCase()}</span> */}
                          <span>{data.email && circleCharacter(data)}</span>
                        </div>
                      </label>
                      <div className="commentContent">
                        {data.message && <p>{data.message}</p>}
                      </div>
                    </div>
                  );
                })}
            </div>
          </div>
          <div className="commentWrap-input">
            <div className="commentContent">
              <Form onSubmit={e => this.onFormSubmit(e)}>
                <FormGroup>
                  <Input
                    type="text"
                    value={comment}
                    onChange={e => {
                      this.onInputChange(e);
                    }}
                    name="comment"
                    placeholder="Write a comment"
                  />
                </FormGroup>
              </Form>
            </div>
          </div>

          <div className="pt-3 pb-3">
            <h3 className="mb-2">Assignee</h3>
            <div className="image-upload">
              <ul className="list-inline image-upload">
                {taskData.assign_to &&
                  taskData.assign_to.length > 0 &&
                  taskData.assign_to.map((task, index) => {
                    return (
                      <li>
                        <label className="image" title={(task && task.email) || ""}>
                          <div className="assigneeUser">
                            {/* <span style={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)' }}>{(task && task.charAt(0).toUpperCase()) || ''}</span> */}
                            <span
                              className="partImg"
                              style={{
                                position: "absolute",
                                top: "50%",
                                left: "50%",
                                transform: "translate(-50%, -50%)"
                              }}
                            >
                              {circleCharacter(task) || ""}
                            </span>
                          </div>
                        </label>
                      </li>
                    );
                  })}
              </ul>
            </div>
          </div>
          <div className="pt-3 pb-3">
            <h3 className="mb-0">
              Add Document{" "}
              <span className="upload-pdf">
                <Label>
                  <input
                    onChange={e => this.handleAddTaskDocument(e)}
                    accept="application/pdf"
                    type="file"
                  />
                  <img src={PlusIcon} alt="" />
                </Label>
              </span>
            </h3>
            <div className="documentAdd">
              <ul className="list-inline document-upload">
                {taskDocuments &&
                  taskDocuments.length > 0 &&
                  taskDocuments.map(document => {
                    return (
                      <li>
                        <Label>
                          <div className="doc-content">
                            <a download href={document.url} target="_blank">
                              <img src={PDFIcon} alt="" />
                              <p title={document.name}>{document.name}</p>
                            </a>
                          </div>
                          <Dropdown
                            isOpen={
                              isOpenDeleteMode &&
                              document.url === openDropdownValue
                            }
                            toggle={() => this.toggle(document.url)}
                            className="edit-icon"
                          >
                            <DropdownToggle>
                              <Edit />
                            </DropdownToggle>
                            <DropdownMenu>
                              <DropdownItem
                                onClick={() => this.handleDelete(document)}
                              >
                                Delete
                              </DropdownItem>
                            </DropdownMenu>
                          </Dropdown>
                        </Label>
                      </li>
                    );
                  })}
                {isUploading && (
                  <li>
                    <div className="doc-content">
                      <span style={{ textAlign: "center" }}>
                        <Loader
                          type="TailSpin"
                          color="#00BFFF"
                          height={60}
                          width={60}
                        />
                        Uploading...
                      </span>
                    </div>
                  </li>
                )}
              </ul>
            </div>
          </div>
          <div className="dateWrapper">
            <Row>
              <Col md={6} lg={3}>
                <FormGroup>
                  <Label for="startDate" className="required">
                    Start date
                  </Label>
                  <Input
                    type="text"
                    readOnly
                    disabled
                    value={
                      taskData.status !== "Not Started"
                        ? StartDate
                          ? StartDate
                          : EndDate
                        : ""
                    }
                    name="startDate"
                    placeholder="Start Date"
                  />
                </FormGroup>
              </Col>
              <Col md={6} lg={3}>
                <FormGroup>
                  <Label for="" className="required">
                    End date
                  </Label>
                  <Input
                    type="text"
                    readOnly
                    disabled
                    value={
                      taskData.status !== "Not Started" &&
                        taskData.status !== "In Progress"
                        ? EndDate
                        : ""
                    }
                    name="endDate"
                    placeholder="End Date"
                  />
                </FormGroup>
              </Col>
              <Col md={6} lg={3}>
                <FormGroup>
                  <Label for="" className="required">
                    Due date
                  </Label>
                  <Input
                    type="text"
                    readOnly
                    disabled
                    value={DueDate}
                    name="dueDate"
                    placeholder="Due Date"
                  />
                </FormGroup>
              </Col>
              <Col md={6} lg={3}>
                <FormGroup>
                  <Label for="" className="required">
                    Status
                  </Label>
                  <Select
                    value={
                      taskOption.value
                        ? { label: taskOption.value, value: taskOption.value }
                        : {
                          value: taskOptions[0].value,
                          label: taskOptions[0].label
                        }
                    }
                    // defaultValue={taskOptions[0]}
                    name="taskOption"
                    className={
                      taskOption.value === "Not Started" ||
                        taskOption.value === ""
                        ? "inProgress"
                        : taskOption.value === "In Progress"
                          ? "notStarted"
                          : taskOption.value === "Completed"
                            ? "completed"
                            : "overDue"
                    }
                    classNamePrefix="status"
                    options={taskOptions}
                    onChange={data => {
                      this.setState({
                        taskOption: data
                      });
                    }}
                  />
                </FormGroup>
              </Col>
            </Row>
          </div>
          <div className="task-btn">
            <Button role="button" onClick={e => this.onSaveTask(e)}>
              Save
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    taskData: state.taskDetails.createTaskData
  };
};
const mapDispatchToProps = dispatch => {
  return bindActionCreators({}, dispatch);
};
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(SidebarContent)
);
