import React, { Component } from "react";
import { FormGroup, Input, Label, Button } from "reactstrap";
import DatePicker from "react-datepicker";
import moment from "moment";

class FilterOptions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentDepartments: [],
      currentProjects: [],
      currentMeetings: [],
      dueDate: null,
      startDate: null,
      endDate: null,
      error: ""
    };
  }
  componentDidMount() {
    const { taskDetails, setCurrentFilters } = this.props;
    if (taskDetails.filterApplied) {
      this.setState({
        currentDepartments: taskDetails.filterApplied ? taskDetails.currentDepartments : [],
        currentProjects: taskDetails.filterApplied ? taskDetails.currentProjects : [],
        currentMeetings: taskDetails.filterApplied ? taskDetails.currentMeetings : [],
        dueDate: taskDetails.dueDateFilters ? new Date(taskDetails.dueDateFilters) : null,
        startDate: taskDetails.startDateArchived ? new Date(taskDetails.startDateArchived) : null,
        endDate: taskDetails.endDateArchived ? new Date(taskDetails.endDateArchived) : null
      });
      // let archivedTasks = taskDetails && taskDetails.archivedTasks || [];
      // let allMeetings = [];
      // if (archivedTasks && archivedTasks.length > 0) {
      //   for (const task of archivedTasks) {
      //     if (task && task.createdAt) {
      //       let date = moment.utc(task.createdAt).toDate();
      //       let taskDate = moment(taskDate).format("ll");
      //       debugger
      //     }
      //     // if (meetingDate !== todayDate) {
      //     //   allMeetings.push(meeting);
      //     // }
      //   }
      //   const obj = {
      //     filteredArchivedTasks: allMeetings
      //   }
      //   setCurrentFilters(obj);
      // }
      // taskDetails && taskDetails.filteredArchivedTasks
    }
  }
  toggleDepratments = data => {
    const { currentDepartments, currentProjects, currentMeetings } = this.state;
    const index = currentDepartments.findIndex(
      item => item._id.department_id === data._id.department_id
    );
    const indexDeleted = currentDepartments.find(
      item => item._id.department_id === data._id.department_id
    );
    let oldDepartments = currentDepartments;
    let newProjects = [];
    let newMeetings = [];

    if (index > -1) {
      oldDepartments.splice(index, 1);
      currentProjects.forEach(item => {
        if (item.department.department_id !== indexDeleted._id.department_id) {
          newProjects.push(item);
        }
      });
      currentMeetings.forEach(meetingData => {
        if (
          meetingData &&
          meetingData.department_id !== indexDeleted._id.department_id
        ) {
          newMeetings.push(meetingData);
        }
      });
      this.setState({
        currentProjects: newProjects,
        currentMeetings: newMeetings
      });
    } else {
      oldDepartments.push(data);
    }

    this.setState({ currentDepartments: oldDepartments });
  };
  toggleProjects = (e, project, department) => {
    const { currentProjects, currentMeetings } = this.state;
    const index = currentProjects.findIndex(
      item => item.project.project_name_id === project.project_name_id
    );
    const meetingData = currentProjects.find(
      item => item.project.project_name_id === project.project_name_id
    );
    let oldProjects = currentProjects;
    let newMeetings = [];
    if (index > -1) {
      oldProjects.splice(index, 1);
      currentMeetings.forEach(item => {
        if (
          meetingData &&
          meetingData.project &&
          item.project_id !== meetingData.project.project_name_id
        ) {
          newMeetings.push(item);
        }
      });
      this.setState({
        currentMeetings: newMeetings
      });
    } else {
      oldProjects.push({ project, department });
    }
    this.setState({
      currentProjects: oldProjects
    });
  };

  toggleMeetings = (meeting, data) => {
    const { currentMeetings } = this.state;
    const index = currentMeetings.findIndex(
      item => item.title_id === meeting.title_id
    );
    let oldMeetings = currentMeetings;
    if (index > -1) {
      oldMeetings.splice(index, 1);
    } else {
      oldMeetings.push({
        title_id: meeting.title_id,
        title: meeting.title,
        department_id: data.department.department_id,
        project_id: data.project.project_name_id
      });
    }
    this.setState({
      currentMeetings: oldMeetings
    });
  };

  clearFilters = e => {
    e.preventDefault();
    const { isUpdated, isFilterApplied } = this.props;
    isFilterApplied(false);
    this.setState({
      currentDepartments: [],
      currentProjects: [],
      currentMeetings: []
    });
    isUpdated(false);
  };
  applyFilters = (e) => {
    e.preventDefault();
    const { currentDepartments, currentProjects, currentMeetings, dueDate, startDate, endDate } = this.state;
    const { applyFilters, userDetails, isFilterApplied, setCurrentFilters, taskDetails } = this.props;
    if (taskDetails && taskDetails.showArchived) {
      if (startDate <= endDate) {
        isFilterApplied(true);
        let archivedTasks = (taskDetails && taskDetails.archivedTasks && taskDetails.archivedTasks.tasks) || [];
        let allMeetings = [];
        if (archivedTasks && archivedTasks.length > 0) {
          for (const task of archivedTasks) {
            if (task && task.createdAt) {
              let taskDate = moment.utc(task.createdAt).toDate();
              taskDate.setHours(0);
              taskDate.setMinutes(0);
              taskDate.setSeconds(0);
              taskDate.setMilliseconds(0);
              if (taskDate) {
                if (taskDate >= startDate && taskDate <= endDate) {
                  allMeetings.push(task);
                }
              }
            }
          }
          const obj = {
            filteredArchivedTasks: allMeetings,
            startDate,
            endDate
          }
          setCurrentFilters(obj);
        }
      }
      else if (startDate > endDate) {
        this.setState({
          error: "End date must be greater than start date"
        })
      }
      else if (!startDate || !endDate) {
        this.setState({
          error: "Please enter date"
        })
      }
    }
    else {
      let departments = [];
      let projects = [];
      let titles = [];
      currentDepartments &&
        currentDepartments.length > 0 &&
        currentDepartments.forEach(data => {
          departments.push(data._id.department);
        });
      currentProjects &&
        currentProjects.length > 0 &&
        currentProjects.forEach(data => {
          projects.push(data.project.project_name);
        });
      currentMeetings &&
        currentMeetings.length > 0 &&
        currentMeetings.forEach(data => {
          titles.push(data.title);
        });

      const obj = {
        email: userDetails.email,
        departments: departments,
        titles: titles,
        project_names: projects,
        due_date: dueDate ? dueDate : null
      };
      applyFilters(obj).then(result => {
        const { status } = result.payload.data;
        if (status === 200) {
          isFilterApplied(true);
          const obj = {
            currentDepartments,
            currentProjects,
            currentMeetings,
            dueDate
          };
          setCurrentFilters(obj);
        }
      });
    }
  };
  render() {
    const { isOpen, filterData, taskDetails } = this.props;
    const {
      currentDepartments,
      currentProjects,
      currentMeetings,
      dueDate,
      startDate,
      endDate,
      error
    } = this.state;
    let isDisabled;
    if (taskDetails && taskDetails.showArchived) {
      isDisabled =
        startDate === null && endDate === null
    }
    else {
      isDisabled =
        currentDepartments.length === 0 &&
        currentProjects.length === 0 &&
        currentMeetings.length === 0 &&
        dueDate === null;
    }

    return (
      <div className={`taskMgmt-filter ${isOpen ? "open" : ""}`}>
        {
          taskDetails.showArchived ? <div>
            <div className="filterWrap">
              <div className="d-flex align-items-center ">
                <h4 className="mb-0">Start Date</h4>
                <DatePicker
                  selected={startDate}
                  onChange={date => {
                    this.setState({
                      startDate: date
                    });
                  }}
                  isClearable
                  showTimeInput={false}
                  dateFormat="d/MM/yyyy"
                  placeholderText="Start date"
                  className="form-control date"
                // minDate={moment().toDate()}
                />
              </div>
              <div className="d-flex align-items-center ml-5">
                <h4 className="mb-0">End Date</h4>
                <DatePicker
                  selected={endDate}
                  onChange={date => {
                    this.setState({
                      endDate: date
                    });
                  }}
                  isClearable
                  showTimeInput={false}
                  dateFormat="d/MM/yyyy"
                  placeholderText="End date"
                  className="form-control date"
                // minDate={moment().toDate()}
                />
              </div>
            </div>
            {error && error.length > 0 && (
              <span className="text-danger">{error}</span>
            )}
          </div>
            : <div>
              <div className="filterWrap">
                <div className="filterLabels d-flex align-items-center ">
                  <h4 className="mb-0">Due Date</h4>
                  <DatePicker
                    selected={dueDate}
                    onChange={date => {
                      this.setState({
                        dueDate: date
                      });
                    }}
                    isClearable
                    showTimeInput={false}
                    dateFormat="d/MM/yyyy"
                    placeholderText="Due date"
                    className="form-control date"
                    minDate={moment().toDate()}
                  />
                </div>
              </div>
              <div className="filterWrap">
                <div className="filterLabels">
                  <h4 className="mb-0">Department</h4>
                </div>
                <div className="d-flex flex-wrap filterOptions">
                  {filterData &&
                    filterData.length > 0 &&
                    filterData.map(data => {
                      const selectedItem = currentDepartments.find(
                        obj => obj._id.department_id === data._id.department_id
                      );
                      return (
                        <FormGroup check>
                          <div onClick={() => this.toggleDepratments(data)}>
                            <Input
                              type="checkbox"
                              checked={
                                selectedItem &&
                                selectedItem._id.department_id ===
                                data._id.department_id
                              }
                            />
                            <Label className="checkbox-icon">
                              {data && data._id && data._id.department
                                ? data._id.department
                                : "Others"}
                            </Label>
                          </div>
                        </FormGroup>
                      );
                    })}
                </div>
              </div>
              {currentDepartments && currentDepartments.length > 0 && (
                <div className="filterWrap">
                  <div className="filterLabels">
                    <h4 className="mb-0">Projects</h4>
                  </div>
                  <div className="d-flex flex-wrap filterOptions">
                    {currentDepartments &&
                      currentDepartments.length > 0 &&
                      currentDepartments.map(data => {
                        return (
                          data &&
                          data.children &&
                          data.children.length > 0 &&
                          data.children.map(project => {
                            const selectedItem = currentProjects.find(
                              item =>
                                item.project.project_name_id ===
                                project.project_name_id
                            );
                            return (
                              <FormGroup check>
                                <div
                                  onClick={e =>
                                    this.toggleProjects(e, project, data._id)
                                  }
                                >
                                  <Input
                                    type="checkbox"
                                    checked={
                                      selectedItem &&
                                      selectedItem.project.project_name_id ===
                                      project.project_name_id
                                    }
                                  />
                                  <Label className="checkbox-icon">
                                    {project.project_name
                                      ? project.project_name
                                      : "Others"}
                                  </Label>
                                </div>
                              </FormGroup>
                            );
                          })
                        );
                      })}
                  </div>
                </div>
              )}
              {currentProjects && currentProjects.length > 0 && (
                <div className="filterWrap">
                  <div className="filterLabels">
                    <h4 className="mb-0">Meetings</h4>
                  </div>
                  <div className="d-flex flex-wrap filterOptions">
                    {currentProjects.map(data => {
                      return (
                        data &&
                        data.project.titles &&
                        data.project.titles.length > 0 &&
                        data.project.titles.map(meeting => {
                          const selectedItem = currentMeetings.find(
                            item => item.title_id === meeting.title_id
                          );
                          return (
                            <FormGroup check>
                              <div onClick={() => this.toggleMeetings(meeting, data)}>
                                <Input
                                  type="checkbox"
                                  checked={
                                    selectedItem &&
                                    selectedItem.title_id === meeting.title_id
                                  }
                                />
                                <Label className="checkbox-icon">
                                  {meeting.title || "Others"}
                                </Label>
                              </div>
                            </FormGroup>
                          );
                        })
                      );
                    })}
                  </div>
                </div>
              )}
            </div>
        }
        <div className="filterSubmittedBtn">
          <Button disabled={isDisabled} onClick={e => this.applyFilters(e)}>
            Apply
          </Button>
          <Button className="clearBtn" onClick={e => this.clearFilters(e)}>
            Clear All
          </Button>
        </div>
      </div >
    );
  }
}

export default FilterOptions;
