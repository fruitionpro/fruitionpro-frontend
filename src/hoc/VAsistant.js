import React from 'react';
import Artyom from '../lib/artyom';
import Commands from '../shared/components/virtualAssistant/Commands';

const VirtualAssistant = new Artyom();

const withVA = (WrappedComponent) => {
	return class VA extends React.Component {
		constructor(props) {
			super(props);
			this.state = {
				artyomActive: false,
				recognizedText: '',
			};
			window.VirtualAssistant = VirtualAssistant;
			let CommandsInjector = new Commands(VirtualAssistant);
			CommandsInjector.injectCommands();
			VirtualAssistant.redirectRecognizedTextOutput((recognized, isFinal) => {
				console.log(isFinal);
				isFinal &&
					this.setState({
						recognizedText: recognized,
					});
			});
			VirtualAssistant.when('ERROR', (error) => {
				console.error(error);
			});
		}

		handleStop = () => {
			VirtualAssistant.fatality().then(() => {
				console.log('Artyom succesfully stopped');
				this.setState({
					artyomActive: false,
				});
			});
		};

		handleInitiate = () => {
			VirtualAssistant.initialize({
				lang: 'en-GB',
				continuous: true,
				soundex: true,
				debug: true,
				speed: 0.9,
				mode: 'normal',
				listen: true,
				obeyKeyword: 'listen to me',
			})
				.then(() => {
					console.log('VirtualAssistant succesfully initialized');
					this.setState({
						artyomActive: true,
					});
				})
				.catch(() => {});
		};

		render() {
			return (
				<WrappedComponent
					{...this.props}
					{...this.state}
					startVA={this.handleInitiate}
					stopVA={this.handleStop}
					VA={VirtualAssistant}
				/>
			);
		}
	};
};

export default withVA;
